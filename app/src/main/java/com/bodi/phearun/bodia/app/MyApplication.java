package com.bodi.phearun.bodia.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.bodi.phearun.bodia.app.di.component.ApplicationComponent;

import com.bodi.phearun.bodia.app.di.component.DaggerApplicationComponent;
import com.bodi.phearun.bodia.app.di.module.ApplicationModule;
import com.bodi.phearun.bodia.service.HandlerNotificationData;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.onesignal.OneSignal;

public class MyApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationReceivedHandler(new HandlerNotificationData(this))
//                .setNotificationOpenedHandler(new HandlerNotificationOpen(this))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        //Facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        applicationComponent = DaggerApplicationComponent.builder().
                applicationModule(new ApplicationModule(this)).build();

    }
    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

}
