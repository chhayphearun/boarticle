package com.bodi.phearun.bodia.app.di.component;

import com.bodi.phearun.bodia.app.di.PerActivity;
import com.bodi.phearun.bodia.app.di.module.ActivityModule;
import com.bodi.phearun.bodia.ui.ViewImageActivity;
import com.bodi.phearun.bodia.ui.addupdatearticle.AddUpdateArticleActivity;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.ui.bookmark.BookMarkActivity;
import com.bodi.phearun.bodia.ui.flashscreen.FlashScreenActivity;
import com.bodi.phearun.bodia.ui.fontsize.FontSizeActivity;
import com.bodi.phearun.bodia.ui.login.LoginActivity;
import com.bodi.phearun.bodia.ui.login.LoginFragment;
import com.bodi.phearun.bodia.ui.main.MainActivity;
import com.bodi.phearun.bodia.ui.main.fragment.ArticleFragment;
import com.bodi.phearun.bodia.ui.readarticle.ReadArticleActivity;
import com.bodi.phearun.bodia.ui.setting.SettingActivity;
import com.bodi.phearun.bodia.ui.signup.SignUpActivity;
import com.bodi.phearun.bodia.ui.fontstyle.FontStyleActivity;

import dagger.Component;


@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(ArticleFragment fragment);

    void inject(AddUpdateArticleActivity activity);

    void inject(SignUpActivity activity);

    void inject(FontSizeActivity activity);

    void inject(ReadArticleActivity activity);

    void inject(FontStyleActivity activity);

    void inject(BookMarkActivity activity);

    void inject(ViewImageActivity activity);

    void inject(LoginFragment fragment);

    void inject(FlashScreenActivity activity);

    void inject(LoginActivity activity);

    void inject(SettingActivity activity);


}
