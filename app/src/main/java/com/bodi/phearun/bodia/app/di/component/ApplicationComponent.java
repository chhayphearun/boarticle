package com.bodi.phearun.bodia.app.di.component;

import com.bodi.phearun.bodia.app.MyApplication;
import com.bodi.phearun.bodia.app.di.module.ApplicationModule;
import com.bodi.phearun.bodia.ui.base.BaseActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(BaseActivity app);
}
