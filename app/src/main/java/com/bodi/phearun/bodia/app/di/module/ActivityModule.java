package com.bodi.phearun.bodia.app.di.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import com.bodi.phearun.bodia.app.di.ActivityContext;
import com.bodi.phearun.bodia.app.di.PerActivity;
import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.app.network.PushNotificationService;
import com.bodi.phearun.bodia.service.ServiceGenerators;
import com.bodi.phearun.bodia.ui.addupdatearticle.mvp.AddUpdatePresenter;
import com.bodi.phearun.bodia.ui.addupdatearticle.mvp.AddUpdatePresenterImple;
import com.bodi.phearun.bodia.ui.addupdatearticle.mvp.AddUpdateView;
import com.bodi.phearun.bodia.ui.bookmark.mvp.BookMarkPresenter;
import com.bodi.phearun.bodia.ui.bookmark.mvp.BookMarkPresenterImple;
import com.bodi.phearun.bodia.ui.bookmark.mvp.BookMarkView;
import com.bodi.phearun.bodia.ui.login.mvp.LoginPresenter;
import com.bodi.phearun.bodia.ui.login.mvp.LoginPresenterImple;
import com.bodi.phearun.bodia.ui.login.mvp.LoginView;
import com.bodi.phearun.bodia.ui.main.mvp.ArticlePresenter;
import com.bodi.phearun.bodia.ui.main.mvp.ArticlePresenterImple;
import com.bodi.phearun.bodia.ui.main.mvp.ArticleView;
import com.bodi.phearun.bodia.ui.readarticle.mvp.AddReBookMarkPresenter;
import com.bodi.phearun.bodia.ui.readarticle.mvp.AddReBookMarkPresenterImple;
import com.bodi.phearun.bodia.ui.readarticle.mvp.AddReBookMarkView;
import com.bodi.phearun.bodia.ui.signup.mvp.SignUpPresenter;
import com.bodi.phearun.bodia.ui.signup.mvp.SignUpPresenterImple;
import com.bodi.phearun.bodia.ui.signup.mvp.SignUpView;
import com.bodi.phearun.bodia.ui.user.mvp.UserPresenter;
import com.bodi.phearun.bodia.ui.user.mvp.UserPresenterImple;
import com.bodi.phearun.bodia.ui.user.mvp.UserView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {
    private AppCompatActivity mActivity;
    private static final String shareName = "NAME@SHARE";
    public ActivityModule(AppCompatActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    ArticlePresenter<ArticleView> provideArticlePresenter(ArticlePresenterImple<ArticleView> presenter) {
        return presenter;
    }
    @Provides
    AddUpdatePresenter<AddUpdateView> provideAddUpdatePresenter(AddUpdatePresenterImple<AddUpdateView> presenter) {
        return presenter;
    }
    @Provides
    SignUpPresenter<SignUpView> provideSignUpPresenter(SignUpPresenterImple<SignUpView> presenter){
        return presenter;
    }
    @Provides
    @PerActivity
    ArticleService getArticleService() {
        return ServiceGenerators.createService(ArticleService.class);
    }
    @Provides
    SharedPreferences provideSharedPrefs(){
        return mActivity.getSharedPreferences(shareName, Context.MODE_PRIVATE);
    }
    @Provides
    @PerActivity
    PushNotificationService providePushNotificationService(){
        return ServiceGenerators.createService(PushNotificationService.class,
                "Basic OTdhNmUyNDMtODdjZS00NzIzLWIzZGItMzFiMWNjY2FlYzEx");
    }
    @Provides
    AddReBookMarkPresenter<AddReBookMarkView> provideAddReBookMarkPresenter(AddReBookMarkPresenterImple<AddReBookMarkView> presenter){
        return presenter;
    }
    @Provides
    BookMarkPresenter<BookMarkView> provideBookMarkPresenter(BookMarkPresenterImple<BookMarkView> presenter){
        return presenter;
    }
    @Provides
    LoginPresenter<LoginView> provideLoginPresenter(LoginPresenterImple<LoginView> presenter){
        return presenter;
    }
    @Provides
    UserPresenter<UserView.userInfo> provideUserInfoPresenter(UserPresenterImple<UserView.userInfo> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

}
