package com.bodi.phearun.bodia.app.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.app.di.ApplicationContext;
import com.bodi.phearun.bodia.app.di.component.ActivityComponent;
import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.service.ServiceGenerators;
import com.bodi.phearun.bodia.util.MySharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

@Module
public class ApplicationModule {
    private final Application mApplication;
    private static final String shareName = "NAME@SHARE";
    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }
    @Singleton
    @Provides
    SharedPreferences provideSharedPrefs(){
        return mApplication.getSharedPreferences(shareName, Context.MODE_PRIVATE);
    }


}
