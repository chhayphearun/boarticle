package com.bodi.phearun.bodia.app.network;

import com.bodi.phearun.bodia.data.PostUser;
import com.bodi.phearun.bodia.data.response.ResponseCategory;
import com.bodi.phearun.bodia.data.ImageResponse;
import com.bodi.phearun.bodia.data.PostArticle;
import com.bodi.phearun.bodia.data.PostBookMark;
import com.bodi.phearun.bodia.data.response.ResponseArticle;
import com.bodi.phearun.bodia.data.response.ResponseGetArticle;
import com.bodi.phearun.bodia.data.response.ReponsePostBookMark;
import com.bodi.phearun.bodia.data.response.ResponseBookMark;
import com.bodi.phearun.bodia.data.response.ResponseGetBookMark;
import com.bodi.phearun.bodia.data.response.UserResponse;


import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {
    //Article
    @GET("/v1/api/articles")
    Observable<ResponseGetArticle> getAllArticle(@Query("page") long page);

    @POST("/v1/api/articles")
    Observable<ResponseArticle> uploadArticle(@Body PostArticle response);

    @DELETE("/v1/api/articles/{id}")
    Observable<ResponseArticle> removeArticle(@Path("id") long id);

    @GET("/v1/api/articles/{id}")
    Observable<ResponseGetArticle> getArticleById(@Path("id") long id);

    @PUT("/v1/api/articles/{id}")
    Observable<ResponseArticle> updateArticle(@Path("id") long id, @Body PostArticle article);
    //GetCategory
    @GET("/v1/api/categories")
    Observable<ResponseCategory> getAllCate();
    //Upload Image
    @Multipart
    @POST("/v1/api/uploadfile/single")
    Observable<ImageResponse> uploadImage(@Part MultipartBody.Part file, @Part("FILE") RequestBody name);
    //User
    @Multipart
    @POST("/v1/api/users")
    Observable<UserResponse> signUp(@Query("EMAIL") String email, @Query("NAME") String name, @Query("PASSWORD") String password,
                                    @Query("GENDER") String gender, @Query("TELEPHONE") String telephone, @Query("FACEBOOK_ID") String facebookId,
                                    @Part MultipartBody.Part file, @Part("PHOTO") RequestBody fileName);

    @POST("/v1/api/authentication")
    Observable<UserResponse> login(@Body PostUser user);

    @GET("/v1/api/users/{userId}")
    Observable<UserResponse> getUserInfo(@Path("userId") long id);

    //wishlists
    @GET("/v1/api/wishlists/{userId}")
    Observable<ResponseGetBookMark> getAllBookMark(@Path("userId") long userId);

    @POST("/v1/api/wishlists")
    Observable<ReponsePostBookMark> addBookMark(@Body PostBookMark bookMark);

    @DELETE("/v1/api/wishlists/{id}")
    Observable<ResponseBookMark> removeBookMark(@Path("id") long id);
}
