package com.bodi.phearun.bodia.app.network;

import com.bodi.phearun.bodia.data.onesignal.OneSignalResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PushNotificationService {
    @POST("api/v1/notifications")
    Observable<OneSignalResponse> pushNotification(@Body OneSignalResponse oneSignalResponse);
}
