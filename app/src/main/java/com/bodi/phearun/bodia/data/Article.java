package com.bodi.phearun.bodia.data;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Article implements Parcelable {
    @Expose
    @SerializedName("IMAGE")
    private String IMAGE;
    @Expose
    @SerializedName("CATEGORY")
    private Category Category;
    @Expose
    @SerializedName("STATUS")
    private String STATUS;
    @Expose
    @SerializedName("AUTHOR")
    private Author Author;
    @Expose
    @SerializedName("CREATED_DATE")
    private String CREATED_DATE;
    @Expose
    @SerializedName("DESCRIPTION")
    private String DESCRIPTION;
    @Expose
    @SerializedName("TITLE")
    private String TITLE;
    @Expose
    @SerializedName("ID")
    private int ID;

    protected Article(Parcel in) {
        IMAGE = in.readString();
        Category = in.readParcelable(Category.class.getClassLoader());
        STATUS = in.readString();
        Author = in.readParcelable(Author.class.getClassLoader());
        CREATED_DATE = in.readString();
        DESCRIPTION = in.readString();
        TITLE = in.readString();
        ID = in.readInt();
        bitmap = in.readParcelable(Bitmap.class.getClassLoader());
        Time = in.readString();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    private Bitmap bitmap;


    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }


    private String Time;


    public Article(){

    }

    public Article(String CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public Article(String CREATED_DATE, String time, int ID, String TITLE, String DESCRIPTION, String IMAGE, Category category, String STATUS, Author author) {
        this.IMAGE = IMAGE;
        Category = category;
        this.STATUS = STATUS;
        Author = author;
        this.CREATED_DATE = CREATED_DATE;
        this.DESCRIPTION = DESCRIPTION;
        this.TITLE = TITLE;
        this.ID = ID;
        Time = time;
    }

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public Category getCategory() {
        return Category == null ? new Category("Null",0) : Category;
    }

    public void setCategory(Category category) {
        this.Category = category;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public Author getAuthor() {
        return Author;
    }

    public void setAuthor(Author author) {
        this.Author = author;
    }

    public String getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(String CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(IMAGE);
        dest.writeParcelable(Category, flags);
        dest.writeString(STATUS);
        dest.writeParcelable(Author, flags);
        dest.writeString(CREATED_DATE);
        dest.writeString(DESCRIPTION);
        dest.writeString(TITLE);
        dest.writeInt(ID);
        dest.writeParcelable(bitmap, flags);
        dest.writeString(Time);
    }
}
