package com.bodi.phearun.bodia.data;

import android.text.TextUtils;

import com.bodi.phearun.bodia.data.Article;

import java.util.ArrayList;

/**
 * Created by fiplus on 6/26/18.
 */

public class ArticleSort {

    public ArrayList<Article> articlesItem;
    public boolean isSection;
    private static String lastHeader;
    private static ArrayList<ArticleSort> sorts = new ArrayList<>();
    private static ArrayList<Article> article = new ArrayList<>();
    public ArticleSort(ArrayList<Article> articlesItem, boolean isSection) {
        this.articlesItem = articlesItem;
        this.isSection = isSection;
    }

    public ArrayList<Article> getArticlesItem() {
        return articlesItem;
    }

    public void setArticlesItem(ArrayList<Article> articlesItem) {
        this.articlesItem = articlesItem;
    }

    public boolean isSection() {
        return isSection;
    }

    public void setSection(boolean section) {
        isSection = section;
    }

    public static String getLastHeader() {
        return lastHeader;
    }

    public static void setLastHeader(String lastHeader) {
        ArticleSort.lastHeader = lastHeader;
    }

    public static ArrayList<ArticleSort> getSorts() {
        return sorts;
    }

    public static void setSorts(ArrayList<ArticleSort> sorts) {
        ArticleSort.sorts = sorts;
    }

    public static ArrayList<Article> getArticle() {
        return article;
    }

    public static void setArticle(ArrayList<Article> article) {
        ArticleSort.article = article;
    }

    public static ArrayList<ArticleSort> getHeaderListLetter(ArrayList<Article> mArticle,boolean isFirst) {
        if(isFirst){
            lastHeader = "";
            sorts.clear();
            article.clear();
        }
        int size = mArticle.size();
        for (int i = 0; i < size; i++) {
            Article item = mArticle.get(i);
            String date =   item.getCREATED_DATE().substring(0,8);
            int id = item.getID();
            String title =   item.getTITLE();
            String description =  item.getDESCRIPTION();
            String image =   item.getIMAGE();
            String time = item.getCREATED_DATE().substring(8);
            Category category = item.getCategory();
            String status = item.getSTATUS();
            Author author = item.getAuthor();
            //-------- CHECK Header Text --------
            if (!TextUtils.equals(lastHeader, date)) {
                lastHeader = date;
                article.add(new Article(date));
                sorts.add(new ArticleSort(article,true));
            }
            //--------Add ALL Data to arraylist--------
            article.add(new Article(date,time,id,title,description,image,category,status,author));
            sorts.add(new ArticleSort(article,false));
        }
        return sorts;
    }
}
