package com.bodi.phearun.bodia.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Author implements Parcelable {
    @Expose
    @SerializedName("IMAGE_URL")
    private String IMAGE_URL;
    @Expose
    @SerializedName("FACEBOOK_ID")
    private String FACEBOOK_ID;
    @Expose
    @SerializedName("STATUS")
    private String STATUS;
    @Expose
    @SerializedName("TELEPHONE")
    private String TELEPHONE;
    @Expose
    @SerializedName("GENDER")
    private String GENDER;
    @Expose
    @SerializedName("EMAIL")
    private String EMAIL;
    @Expose
    @SerializedName("NAME")
    private String NAME;
    @Expose
    @SerializedName("ID")
    private int ID;

    public Author() {
    }

    protected Author(Parcel in) {
        IMAGE_URL = in.readString();
        FACEBOOK_ID = in.readString();
        STATUS = in.readString();
        TELEPHONE = in.readString();
        GENDER = in.readString();
        EMAIL = in.readString();
        NAME = in.readString();
        ID = in.readInt();
    }

    public static final Creator<Author> CREATOR = new Creator<Author>() {
        @Override
        public Author createFromParcel(Parcel in) {
            return new Author(in);
        }

        @Override
        public Author[] newArray(int size) {
            return new Author[size];
        }
    };

    public String getIMAGE_URL() {
        return IMAGE_URL;
    }

    public void setIMAGE_URL(String IMAGE_URL) {
        this.IMAGE_URL = IMAGE_URL;
    }

    public String getFACEBOOK_ID() {
        return FACEBOOK_ID;
    }

    public void setFACEBOOK_ID(String FACEBOOK_ID) {
        this.FACEBOOK_ID = FACEBOOK_ID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getTELEPHONE() {
        return TELEPHONE;
    }

    public void setTELEPHONE(String TELEPHONE) {
        this.TELEPHONE = TELEPHONE;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getNAME() {
        if(NAME==null){
            NAME = "No Auth Name";
        }
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(IMAGE_URL);
        parcel.writeString(FACEBOOK_ID);
        parcel.writeString(STATUS);
        parcel.writeString(TELEPHONE);
        parcel.writeString(GENDER);
        parcel.writeString(EMAIL);
        parcel.writeString(NAME);
        parcel.writeInt(ID);
    }
}
