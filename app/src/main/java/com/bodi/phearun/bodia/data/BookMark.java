package com.bodi.phearun.bodia.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.bodi.phearun.bodia.data.Article;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookMark implements Parcelable {
    @Expose
    @SerializedName("STATUS")
    private String STATUS;
    @Expose
    @SerializedName("CREATED_DATE")
    private String CREATED_DATE;
    @Expose
    @SerializedName("ARTICLE")
    private Article ARTICLE;
    @Expose
    @SerializedName("USER_ID")
    private int USER_ID;
    @Expose
    @SerializedName("ID")
    private int ID;

    protected BookMark(Parcel in) {
        STATUS = in.readString();
        CREATED_DATE = in.readString();
        ARTICLE = in.readParcelable(Article.class.getClassLoader());
        USER_ID = in.readInt();
        ID = in.readInt();
    }

    public static final Creator<BookMark> CREATOR = new Creator<BookMark>() {
        @Override
        public BookMark createFromParcel(Parcel in) {
            return new BookMark(in);
        }

        @Override
        public BookMark[] newArray(int size) {
            return new BookMark[size];
        }
    };

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(String CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public Article getARTICLE() {
        return ARTICLE;
    }

    public void setARTICLE(Article ARTICLE) {
        this.ARTICLE = ARTICLE;
    }

    public int getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(int USER_ID) {
        this.USER_ID = USER_ID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(STATUS);
        dest.writeString(CREATED_DATE);
        dest.writeParcelable(ARTICLE, flags);
        dest.writeInt(USER_ID);
        dest.writeInt(ID);
    }
}
