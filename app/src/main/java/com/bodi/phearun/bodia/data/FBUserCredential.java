package com.bodi.phearun.bodia.data;

import android.os.Parcel;
import android.os.Parcelable;

public class FBUserCredential implements Parcelable {
    private String id;
    private String first_name;
    private String last_name;
    private FbData picture;
    private String email;

    protected FBUserCredential(Parcel in) {
        id = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        picture = in.readParcelable(FbData.class.getClassLoader());
        email = in.readString();
    }

    public static final Creator<FBUserCredential> CREATOR = new Creator<FBUserCredential>() {
        @Override
        public FBUserCredential createFromParcel(Parcel in) {
            return new FBUserCredential(in);
        }

        @Override
        public FBUserCredential[] newArray(int size) {
            return new FBUserCredential[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public FbData getPicture() {
        return picture;
    }

    public void setPicture(FbData picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(first_name);
        parcel.writeString(last_name);
        parcel.writeParcelable(picture, i);
        parcel.writeString(email);
    }
}
