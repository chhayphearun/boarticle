package com.bodi.phearun.bodia.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostBookMark {

    @Expose
    @SerializedName("ARTICLE_ID")
    private int ARTICLE_ID;
    @Expose
    @SerializedName("USER_ID")
    private int USER_ID;

    public int getARTICLE_ID() {
        return ARTICLE_ID;
    }

    public void setARTICLE_ID(int ARTICLE_ID) {
        this.ARTICLE_ID = ARTICLE_ID;
    }

    public int getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(int USER_ID) {
        this.USER_ID = USER_ID;
    }

    public PostBookMark(int ARTICLE_ID, int USER_ID) {
        this.ARTICLE_ID = ARTICLE_ID;
        this.USER_ID = USER_ID;
    }
}
