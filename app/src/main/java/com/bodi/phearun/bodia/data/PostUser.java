package com.bodi.phearun.bodia.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostUser {
    @Expose
    @SerializedName("EMAIL")
    private String email;
    @Expose
    @SerializedName("PASSWORD")
    private String password;

    public PostUser(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
