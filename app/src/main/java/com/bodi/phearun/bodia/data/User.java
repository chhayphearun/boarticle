package com.bodi.phearun.bodia.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {
    @Expose
    @SerializedName("FACEBOOK_ID")
    private String FACEBOOK_ID;
    @Expose
    @SerializedName("STATUS")
    private String STATUS;
    @Expose
    @SerializedName("TELEPHONE")
    private String TELEPHONE;
    @Expose
    @SerializedName("GENDER")
    private String GENDER;
    @Expose
    @SerializedName("EMAIL")
    private String EMAIL;
    @Expose
    @SerializedName("NAME")
    private String NAME;
    @Expose
    @SerializedName("ID")
    private int ID;

    @Expose
    @SerializedName("IMAGE_URL")
    private String imageUrl;

    public User() {
    }

    public User(String FACEBOOK_ID, String STATUS, String TELEPHONE, String GENDER, String EMAIL, String NAME, int ID, String imageUrl) {
        this.FACEBOOK_ID = FACEBOOK_ID;
        this.STATUS = STATUS;
        this.TELEPHONE = TELEPHONE;
        this.GENDER = GENDER;
        this.EMAIL = EMAIL;
        this.NAME = NAME;
        this.ID = ID;
        this.imageUrl = imageUrl;
    }

    protected User(Parcel in) {
        FACEBOOK_ID = in.readString();
        STATUS = in.readString();
        TELEPHONE = in.readString();
        GENDER = in.readString();
        EMAIL = in.readString();
        NAME = in.readString();
        ID = in.readInt();
        imageUrl = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(FACEBOOK_ID);
        dest.writeString(STATUS);
        dest.writeString(TELEPHONE);
        dest.writeString(GENDER);
        dest.writeString(EMAIL);
        dest.writeString(NAME);
        dest.writeInt(ID);
        dest.writeString(imageUrl);
    }

    public String getFACEBOOK_ID() {
        return FACEBOOK_ID;
    }

    public void setFACEBOOK_ID(String FACEBOOK_ID) {
        this.FACEBOOK_ID = FACEBOOK_ID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getTELEPHONE() {
        return TELEPHONE;
    }

    public void setTELEPHONE(String TELEPHONE) {
        this.TELEPHONE = TELEPHONE;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public static Creator<User> getCREATOR() {
        return CREATOR;
    }
}
