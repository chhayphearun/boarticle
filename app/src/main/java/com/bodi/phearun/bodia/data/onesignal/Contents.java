package com.bodi.phearun.bodia.data.onesignal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contents {
    @Expose
    @SerializedName("en")
    private String en;

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }
}
