package com.bodi.phearun.bodia.data.onesignal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OneSignalResponse {
    @Expose
    @SerializedName("contents")
    private Contents contents;
    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("included_segments")
    private List<String> included_segments;
    @Expose
    @SerializedName("app_id")
    private String app_id;

    public Contents getContents() {
        return contents;
    }

    public void setContents(Contents contents) {
        this.contents = contents;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<String> getIncluded_segments() {
        return included_segments;
    }

    public void setIncluded_segments(List<String> included_segments) {
        this.included_segments = included_segments;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    @Override
    public String toString() {
        return "OneSignalResponse{" +
                "contents=" + contents.getEn() +
                ", data=" + data.getArticle() +
                ", included_segments=" + included_segments +
                ", app_id='" + app_id + '\'' +
                '}';
    }
}
