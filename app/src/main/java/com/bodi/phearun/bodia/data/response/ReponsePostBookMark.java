package com.bodi.phearun.bodia.data.response;

import com.bodi.phearun.bodia.data.BookMark;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReponsePostBookMark {

    @Expose
    @SerializedName("DATA")
    private com.bodi.phearun.bodia.data.BookMark BookMark;
    @Expose
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @Expose
    @SerializedName("CODE")
    private String CODE;

    public BookMark getBookMark() {
        return BookMark;
    }

    public void setBookMark(BookMark bookMark) {
        this.BookMark = bookMark;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }
}
