package com.bodi.phearun.bodia.data.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.bodi.phearun.bodia.data.Article;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseArticle implements Parcelable {
    @Expose
    @SerializedName("DATA")
    private com.bodi.phearun.bodia.data.Article Article;
    @Expose
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @Expose
    @SerializedName("CODE")
    private String CODE;
    public ResponseArticle(){}
    protected ResponseArticle(Parcel in) {
        Article = in.readParcelable(Article.class.getClassLoader());
        MESSAGE = in.readString();
        CODE = in.readString();
    }

    public static final Creator<ResponseArticle> CREATOR = new Creator<ResponseArticle>() {
        @Override
        public ResponseArticle createFromParcel(Parcel in) {
            return new ResponseArticle(in);
        }

        @Override
        public ResponseArticle[] newArray(int size) {
            return new ResponseArticle[size];
        }
    };

    public Article getArticle() {
        return Article;
    }

    public void setArticle(Article article) {
        Article = article;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(Article, i);
        parcel.writeString(MESSAGE);
        parcel.writeString(CODE);
    }
}
