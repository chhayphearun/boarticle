package com.bodi.phearun.bodia.data.response;

import com.bodi.phearun.bodia.data.BookMark;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetBookMark {

    @Expose
    @SerializedName("DATA")
    private List<BookMark> ArticleBookMark;
    @Expose
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @Expose
    @SerializedName("CODE")
    private String CODE;

    @Expose
    @SerializedName("PAGINATION")
    private String PAGINATION;

    public List<BookMark> getArticleBookMark() {
        return ArticleBookMark;
    }

    public void setArticleBookMark( List<BookMark> articleBookMark) {
        ArticleBookMark = articleBookMark;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getPAGINATION() {
        return PAGINATION;
    }

    public void setPAGINATION(String PAGINATION) {
        this.PAGINATION = PAGINATION;
    }
}
