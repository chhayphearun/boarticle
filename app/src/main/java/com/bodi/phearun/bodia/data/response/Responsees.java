package com.bodi.phearun.bodia.data.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Responsees<t> implements Parcelable {
    @Expose
    @SerializedName("DATA")
    private t t;
    @Expose
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @Expose
    @SerializedName("CODE")
    private String CODE;

    protected Responsees(Parcel in) {
        MESSAGE = in.readString();
        CODE = in.readString();
    }

    public static final Creator<Responsees> CREATOR = new Creator<Responsees>() {
        @Override
        public Responsees createFromParcel(Parcel in) {
            return new Responsees(in);
        }

        @Override
        public Responsees[] newArray(int size) {
            return new Responsees[size];
        }
    };

    public t getData() {
        return t;
    }

    public void setData(t t) {
        this.t = t;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(MESSAGE);
        parcel.writeString(CODE);
    }
}
