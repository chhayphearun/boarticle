package com.bodi.phearun.bodia.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bodi.phearun.bodia.R;

import java.util.ArrayList;

public class DialogAdatper extends BaseAdapter {
    private ArrayList<String> arrayList;
    private LayoutInflater inflater;
    private int position;
    //Create constructor
    public DialogAdatper(Context context, ArrayList<String> arrayList,int position) {
        this.arrayList = arrayList;
        inflater = LayoutInflater.from(context);
        this.position = position;
    }
    //return number of item
    @Override
    public int getCount() {
        return arrayList.size();
    }
    //bind custom view to list
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View v = inflater.inflate(R.layout.bottomdialog_item,null);
        TextView text = v.findViewById(R.id.item);
        text.setText(arrayList.get(i));
        ImageView imageView = v.findViewById(R.id.checkImage);
        if(position == i){
            imageView.setVisibility(View.VISIBLE);
        }else{
            imageView.setVisibility(View.GONE);
        }
        return v;
    }
    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


}
