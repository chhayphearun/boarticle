package com.bodi.phearun.bodia.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.ui.login.LoginActivity;

import java.util.ArrayList;

public class DialogItem extends BottomSheetDialogFragment {
    private ArrayList<String> list = new ArrayList<>();
    private onDialogItemClick onDialogItemClick;
    private  int p[];
    private int mPosition;
    private int type;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof onDialogItemClick){
            onDialogItemClick = (onDialogItemClick)context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null){
            list = bundle.getStringArrayList("ITEM_LIST");
            mPosition = bundle.getInt("position");
            type = bundle.getInt("type");
        }
    }
    public static DialogItem openDialog(ArrayList<String> list,int position,int type) {
        DialogItem fragment = new DialogItem();
        Bundle args = new Bundle();
        args.putStringArrayList("ITEM_LIST", list);
        args.putInt("position", position);
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_dialoglist,container,false);
        ListView listView = view.findViewById(R.id.list);
        final DialogAdatper adatper = new DialogAdatper(getActivity(),list,mPosition);
        listView.setAdapter(adatper);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position!=mPosition){
                    onDialogItemClick.onItemClicked(position,type);
                }
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
