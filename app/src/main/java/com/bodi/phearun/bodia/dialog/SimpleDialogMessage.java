package com.bodi.phearun.bodia.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

public class SimpleDialogMessage extends DialogFragment {
    private OnClickOkButton onClickOkButton;
    private String message;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnClickOkButton){
            onClickOkButton = (OnClickOkButton)context;
        }
    }
    public static SimpleDialogMessage openDialog(String message){
        SimpleDialogMessage fragment = new SimpleDialogMessage();
        Bundle bundle = new Bundle();
        bundle.putString("message",message);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null){
            message = bundle.getString("message");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message).setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onClickOkButton.onClickOK();
            }
        });
        return builder.create();
    }
}
