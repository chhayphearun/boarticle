package com.bodi.phearun.bodia.service;



import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.onesignal.Data;
import com.bodi.phearun.bodia.ui.readarticle.ReadArticleActivity;

import java.security.Provider;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AppNotificationService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Article article = intent.getParcelableExtra("article");
        intent = new Intent(this, ReadArticleActivity.class);
        intent.putExtra("tap",true);
        intent.putExtra("readArticle",article);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        CreateNotification(intent,article.getTITLE(),article.getDESCRIPTION());
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
    private void CreateNotification(Intent intent, String title, String body){
        String CHANNEL_ID = "my_channel_01";
        String date = new SimpleDateFormat("ddHmmssS").format(Calendar.getInstance().getTime());
        int id = Integer.parseInt(date);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        // this is a my insertion looking for a solution
        int icon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? R.drawable.applogo: R.drawable.applogo;
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setSmallIcon(icon)
                .setColor(getResources().getColor(R.color.black))
                .setContentTitle(title)
                .setContentText(body)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            assert notificationManager != null;
            int importance = NotificationManager.IMPORTANCE_HIGH;
            CharSequence name = "Notification";
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        startForeground(0,notificationBuilder.build());
        assert notificationManager != null;
        notificationManager.notify(id/* ID of notification */, notificationBuilder.build());
    }
}
