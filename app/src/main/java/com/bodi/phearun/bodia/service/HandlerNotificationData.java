package com.bodi.phearun.bodia.service;

import android.app.Application;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.bodi.phearun.bodia.data.onesignal.Data;
import com.bodi.phearun.bodia.ui.main.fragment.ArticleFragment;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class HandlerNotificationData implements OneSignal.NotificationReceivedHandler {
    private Application application;
    public static final String REQUEST_ACCEPT = "YES";
    private Data article;
    public HandlerNotificationData(Application application) {
        this.application = application;
    }

    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;

        String customKey;

        if (data != null) {
            JsonParser parser = new JsonParser();
            JsonElement mJson =  parser.parse(data.toString());
            Gson gson = new Gson();
            article = gson.fromJson(mJson, Data.class);
//            if(article.getArticle()!=null)
//                Log.i("OneSignalExample", "customkey set with value: " + article.getArticle());

        }
//        Log.i("DataReceive",notification.payload.title);
//        SharedPreferences DataReceivesharedPref = application.getApplicationContext().getSharedPreferences("SHARE",Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString("data",notification.payload.title);
//        editor.apply();
//        String title = notification.payload.title;
//        String body = notification.payload.body;
//
////        Log.i("OneSignalExample", "customkey set with value: " + title);
////        Log.i("OneSignalExample", "customkey set with value: " + body);
        if(article.getArticle()!=null){
            if(notification.isAppInFocus && ArticleFragment.isArticleFragment){
                LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(application.getBaseContext());
                Intent intent = new Intent(REQUEST_ACCEPT);
                intent.putExtra("type", article.getArticle().getCategory().getNAME());
                intent.putExtra("title", article.getArticle().getTITLE());
                broadcaster.sendBroadcast(intent);
            }else {
                Intent myIntent = new Intent(application,AppNotificationService.class);
                myIntent.putExtra("article",article.getArticle());
                application.startService(myIntent);
            }
        }

    }

}
