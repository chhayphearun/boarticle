package com.bodi.phearun.bodia.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.ui.base.BaseActivity;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewImageActivity extends BaseActivity {
    @BindView(R.id.image)
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        ButterKnife.bind(this);
        getActivityComponent().inject(this);

        Intent intent = getIntent();
        if(intent!=null){
            if(intent.getExtras()!=null){
                boolean isProfile = intent.getBooleanExtra("isProfile",false);
                if(isProfile){
                    byte[] byteArray = intent.getByteArrayExtra("image");
                    Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    imageView.setImageBitmap(bmp);
                }else{
                    File file = (File) intent.getExtras().get("image");
                    if(file!=null)
                        if(file.exists()){
                            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                            imageView.setImageBitmap(myBitmap);
                        }
                }

            }

        }
    }
    @OnClick(R.id.close)
    void onClose(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
