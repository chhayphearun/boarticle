package com.bodi.phearun.bodia.ui.addupdatearticle;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.Category;
import com.bodi.phearun.bodia.data.PostArticle;
import com.bodi.phearun.bodia.ui.ViewImageActivity;
import com.bodi.phearun.bodia.ui.addupdatearticle.dialog.BottomDialog;
import com.bodi.phearun.bodia.ui.addupdatearticle.dialog.DialogOnClick;
import com.bodi.phearun.bodia.ui.addupdatearticle.mvp.AddUpdatePresenter;
import com.bodi.phearun.bodia.ui.addupdatearticle.mvp.AddUpdateView;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.util.LocaleHelper;
import com.bodi.phearun.bodia.util.Util;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddUpdateArticleActivity extends BaseActivity implements DialogOnClick,AddUpdateView {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.mSwitch)
    Switch aSwitch;
    @BindView(R.id.imgPicked)
    ImageView imagePicked;
    @BindView(R.id.decorItem)
    LinearLayout decorItem;
    @BindView(R.id.btPickImage)
    Button btPickImage;
    @BindView(R.id.btAdd)
    Button btAdd;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @Inject
    AddUpdatePresenter<AddUpdateView> presenter;

    public static final int CAMERA_REQUEST = 10;
    public static final int RequestPermissionCode = 11;
    public static final int ADD = 0;
    public static final int UPDATE = 1;
    private String imageFilePath;
    private File file;
    private Article article;
    private int position;
    private boolean isUpdate = false;
    private PostArticle postArticle;
    private String imageUrlOld;
    private ArrayList<Category> categories;
    private int cateId=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);
        ButterKnife.bind(this);
        getActivityComponent().inject(this);
        toolbar.setNavigationIcon(R.drawable.ic_arrowback_white);
        setTitle(getResources().getString(R.string.addArticle));
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter.onAttach(this);
        presenter.onGetAllCategory();
        Intent i = getIntent();
        if(i.getExtras()!=null){
            article = i.getParcelableExtra("article");
            position = i.getIntExtra("position",0);
            if(article!=null){
                setTitle(getResources().getString(R.string.updateArticle));
                isUpdate = true;
                setVisDecorItem();
                aSwitch.setVisibility(View.GONE);
                btAdd.setText(getResources().getString(R.string.update));
                title.setText(article.getTITLE());
                description.setText(article.getDESCRIPTION());
                spinner.setSelection(article.getCategory().getID());
                Glide.with(this)
                        .load(article.getIMAGE())
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_launcher_foreground))
                        .into(imagePicked);
                imageUrlOld = article.getIMAGE();
            }
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cateId = categories.get(i).getID();
            }
            
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));

    }
    //---clickEvent---
    @OnClick(R.id.frameImage)
    void onClickFrameImage(){
        selectOption();
    }
    @OnClick(R.id.btPickImage)
    void onClickBtPickImage(){
        if(checkPermission()){
            openBottomDialog();
        }else{
            requestPermission();
        }
    }
    @OnClick(R.id.btAdd)
    void onClickAdd(){
        if(isUpdate){
            if(file!=null){
                presenter.onUploadImage(file,UPDATE);
            }else{
                String mTitle = title.getText().toString();
                String mDesc = description.getText().toString();
                postArticle = setArticle(1,1,mTitle,mDesc,imageUrlOld,"1");
                presenter.onUpdateArticle(article.getID(), postArticle);
            }
        }else{
            if(file==null){
                String mTitle = title.getText().toString();
                String mDesc = description.getText().toString();
                postArticle = setArticle(1,1,mTitle,mDesc,imageUrlOld,"1");
                presenter.onUploadArticle(postArticle);
            }else{
                presenter.onUploadImage(file,ADD);
            }
        }
    }
    //----endClickEvent---

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case RequestPermissionCode:
                if(grantResults.length>0){
                    boolean Camera = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadExternalStorage = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if(Camera && ReadExternalStorage && WriteExternalStorage){
                        openBottomDialog();
                    }else{
                        Toast.makeText(this, "Permission Denied.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == 1 && data!=null){
            Uri uri = data.getData();
            String name = getRealPathFromURI(uri);
            File tempFile = new File(this.getFilesDir().getAbsolutePath(), name);
            //Copy URI contents into temporary file.
            try {
                if(tempFile.createNewFile()){
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(tempFile));
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    os.close();
                }
            }
            catch (IOException e) {
                //Log Error
                e.printStackTrace();
            }
            //Now fetch the new URI
            file = tempFile;
            Uri newUri = Uri.fromFile(tempFile);
            imagePicked.setImageURI(newUri);
            setVisDecorItem();
        }else if(requestCode == CAMERA_REQUEST){
            if(resultCode== RESULT_OK){
                file = new  File(imageFilePath);
                imagePicked.setImageURI(Uri.parse(imageFilePath));
                setVisDecorItem();
            }
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(presenter!=null)
            presenter.onDetach();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onDialogItemPick(int i) {
        if(i==1){
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent,1);
        }else if(i==0){
            openCameraIntent();
        }
    }

    @Override
    public void onUploadArticleSuccess(Article article) {
        Intent intent = new Intent();
        intent.putExtra("article",article);
        setResult(ADD,intent);
        finish();
    }

    @Override
    public void onUpdateSuccess(Article article) {
        Intent intent = new Intent();
        intent.putExtra("article",article);
        intent.putExtra("position",position);
        setResult(UPDATE,intent);
        finish();
    }

    @Override
    public void onUploadImageSuccess(String Image, int type) {
        String mTitle = title.getText().toString();
        String mDesc = description.getText().toString();
        postArticle = setArticle(1,cateId,mTitle,mDesc,Image,"1");
        if(type == UPDATE){
            presenter.onUpdateArticle(article.getID(),postArticle);
        }else if(type == ADD){
            presenter.onUploadArticle(postArticle);
        }
    }

    @Override
    public void onGetAllCategorySuccess(ArrayList<Category> categories) {
        this.categories =categories;
        List<String> cateName = new ArrayList<>();
        for(int in =0; in<categories.size();in++){
            cateName.add(categories.get(in).getNAME());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.item_spinner,cateName);
        spinner.setAdapter(adapter);
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void onShowLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading() {
        progressBar.setVisibility(View.GONE);
    }
    //------------------method------------------

    private void setVisDecorItem(){
        if(decorItem.getVisibility() == View.VISIBLE){
            decorItem.setVisibility(View.GONE);
        }
        if(decorItem.getVisibility() == View.GONE){
            btPickImage.setVisibility(View.VISIBLE);
        }
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    private void openBottomDialog(){
        BottomDialog bottomDialog = new BottomDialog();
        FragmentManager fragmentManager = getSupportFragmentManager();
        bottomDialog.show(fragmentManager,"");
    }
    private File createImageFile() throws IOException {
        File image;
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new java.util.Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        imageFilePath = image.getAbsolutePath();
        return image;
    }
    private void openCameraIntent() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile;
            try {
                photoFile = createImageFile();
            }
            catch (IOException e) {
                e.printStackTrace();
                return;
            }
            Uri photoUri = FileProvider.getUriForFile(this, getPackageName() +".provider", photoFile);
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            setResult(RESULT_OK,pictureIntent);
            startActivityForResult(pictureIntent, CAMERA_REQUEST);
        }
    }

    private PostArticle setArticle(int authId,int cateId,String title,String desc,String imageUrl,String status){
        postArticle = new PostArticle();
        postArticle.setAUTHOR(authId);
        postArticle.setCATEGORY_ID(cateId);
        postArticle.setTITLE(title);
        postArticle.setDESCRIPTION(desc);
        postArticle.setIMAGE(imageUrl);
        postArticle.setSTATUS(status);
        return postArticle;
    }

    void selectOption(){
        if(checkPermission()){
            if(imagePicked.getDrawable()!=null){
                Intent intent = new Intent(AddUpdateArticleActivity.this,ViewImageActivity.class);
                intent.putExtra("image",file);
                startActivity(intent);
            }else{
                openBottomDialog();
            }
        }else{
            requestPermission();
        }
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }
        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]
                {
                        CAMERA,
                        WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE
                }, RequestPermissionCode);
    }
    private boolean checkPermission(){
        int cameraPermission = ContextCompat.checkSelfPermission(getApplicationContext(),CAMERA);
        int writeStorage = ContextCompat.checkSelfPermission(getApplicationContext(),WRITE_EXTERNAL_STORAGE);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(),READ_EXTERNAL_STORAGE);
        return cameraPermission == PackageManager.PERMISSION_GRANTED &&
                writeStorage == PackageManager.PERMISSION_GRANTED &&
                readStorage == PackageManager.PERMISSION_GRANTED;
    }
}
