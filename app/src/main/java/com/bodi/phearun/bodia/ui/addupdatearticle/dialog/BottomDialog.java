package com.bodi.phearun.bodia.ui.addupdatearticle.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.bodi.phearun.bodia.R;

import java.util.ArrayList;

//import com.bodi.phearun.bodia.R;

public class BottomDialog extends BottomSheetDialogFragment {
    private DialogOnClick dialogOnClick;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof DialogOnClick)
        dialogOnClick = (DialogOnClick)context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_dialoglist,container,false);
        ListView listView = view.findViewById(R.id.list);
        ArrayList<String> list = new ArrayList<>();
        list.add(getResources().getString(R.string.takePhoto));
        list.add(getResources().getString(R.string.pickImage));
        final DialogBottomAdapter adatper = new DialogBottomAdapter(getActivity(),list);
        listView.setAdapter(adatper);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogOnClick.onDialogItemPick(position);
                dismiss();
            }
        });
        return view;
    }

}
