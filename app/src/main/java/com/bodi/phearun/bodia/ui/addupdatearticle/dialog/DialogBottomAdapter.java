package com.bodi.phearun.bodia.ui.addupdatearticle.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bodi.phearun.bodia.R;

import java.util.ArrayList;

public class DialogBottomAdapter extends BaseAdapter {
    private ArrayList<String> arrayList;
    private LayoutInflater inflater;
    //Create constructor
    public DialogBottomAdapter(Context context, ArrayList<String> arrayList) {
        this.arrayList = arrayList;
        inflater = LayoutInflater.from(context);
    }
    //return number of item
    @Override
    public int getCount() {
        return arrayList.size();
    }
    //bind custom view to list
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View v = inflater.inflate(R.layout.dialog_bottom,null);
        TextView text = v.findViewById(R.id.item);
        text.setText(arrayList.get(i));
        return v;
    }
    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


}
