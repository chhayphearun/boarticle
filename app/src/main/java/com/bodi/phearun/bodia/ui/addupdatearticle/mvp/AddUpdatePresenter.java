package com.bodi.phearun.bodia.ui.addupdatearticle.mvp;

import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.Category;
import com.bodi.phearun.bodia.data.PostArticle;
import com.bodi.phearun.bodia.ui.base.MvpPresenter;

import java.io.File;
import java.util.ArrayList;

public interface AddUpdatePresenter <V extends AddUpdateView> extends MvpPresenter<V> {
    void onUploadArticle(PostArticle article);
    void onUpdateArticle(long id, PostArticle article);
    void onUploadImage(File imageFile, int type);
    void onGetAllCategory();
}
