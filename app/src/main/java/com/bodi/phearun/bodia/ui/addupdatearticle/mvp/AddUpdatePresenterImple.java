package com.bodi.phearun.bodia.ui.addupdatearticle.mvp;

import android.util.Log;

import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.app.network.PushNotificationService;
import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.Category;
import com.bodi.phearun.bodia.data.onesignal.Contents;
import com.bodi.phearun.bodia.data.onesignal.Data;
import com.bodi.phearun.bodia.data.onesignal.OneSignalResponse;
import com.bodi.phearun.bodia.data.response.ResponseCategory;
import com.bodi.phearun.bodia.data.ImageResponse;
import com.bodi.phearun.bodia.data.PostArticle;
import com.bodi.phearun.bodia.data.response.ResponseArticle;
import com.bodi.phearun.bodia.service.ServiceGenerators;
import com.bodi.phearun.bodia.ui.base.BasePresenter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddUpdatePresenterImple <V extends AddUpdateView> extends BasePresenter<V>
        implements AddUpdatePresenter<V> {
    private ArticleService service;
    private Article mArticle = new Article();
    private String url;
    private CompositeDisposable disposable;
    private PushNotificationService serviceNoti;
    @Inject
    public AddUpdatePresenterImple(ArticleService service, CompositeDisposable disposable
    ,PushNotificationService serviceNoti) {
        this.service = service;
        this.disposable = disposable;
        this.serviceNoti = serviceNoti;
    }

    @Override
    public void onUploadArticle(PostArticle article) {
        uploadArticle(article);
    }
    private void uploadArticle(PostArticle article){
        disposable.add(service.uploadArticle(article)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ResponseArticle>() {
                    @Override
                    public void onNext(ResponseArticle res) {
                        mArticle = res.getArticle();


                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        sendNotification(mArticle);
                        getMvpView().onUploadArticleSuccess(mArticle);
                    }
                }));

    }
    private void sendNotification(Article mArticle){
        OneSignalResponse oneSignalResponse = new OneSignalResponse();
        oneSignalResponse.setApp_id("3d938455-1cd0-4f07-9132-0d9fe9b3c315");
        List<String> included_segments = new ArrayList<>();
        included_segments.add("All");
        oneSignalResponse.setIncluded_segments(included_segments);
        Data data = new Data();
        data.setArticle(mArticle);
        oneSignalResponse.setData(data);
        Contents contents = new Contents();
        contents.setEn("English");
        oneSignalResponse.setContents(contents);
        disposable.add(serviceNoti.pushNotification(oneSignalResponse)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<OneSignalResponse>(){
                    @Override
                    public void onNext(OneSignalResponse oneSignalResponse) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void onUpdateArticle(long id, PostArticle article) {
        onUpdate(id,article);
    }

    @Override
    public void onUploadImage(File imageFile, int type) {
        uploadImage(imageFile,type);
    }

    @Override
    public void onGetAllCategory() {
        getAllCategory();
    }
    private void getAllCategory(){
        getMvpView().onShowLoading();
        final ArrayList<Category> categories = new ArrayList<>();
        disposable.add(service.getAllCate()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ResponseCategory>() {
                    @Override
                    public void onNext(ResponseCategory responseCategory) {
                        categories.addAll(responseCategory.getCategories());
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                        getMvpView().onHideLoading();
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().onGetAllCategorySuccess(categories);
                        getMvpView().onHideLoading();
                    }
                }));
    }
    private void uploadImage(File imageFile, final int type){
        getMvpView().onShowLoading();
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"),imageFile);
        MultipartBody.Part fileUpload = MultipartBody.Part.createFormData("FILE",imageFile.getName(),requestBody);
        RequestBody fileName = RequestBody.create(MediaType.parse("text/plain"),imageFile.getName());
        disposable.add(service.uploadImage(fileUpload,fileName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ImageResponse>() {
                    @Override
                    public void onNext(ImageResponse imageResponse) {
                        url = imageResponse.getData();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                        getMvpView().onHideLoading();
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().onUploadImageSuccess(url,type);
                        getMvpView().onHideLoading();
                    }
                }));

    }
    private void onUpdate(long id, PostArticle article){
        getMvpView().onShowLoading();
        disposable.add(service.updateArticle(id,article)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ResponseArticle>() {
                    @Override
                    public void onNext(ResponseArticle responseArticle) {
                        mArticle = responseArticle.getArticle();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                        getMvpView().onHideLoading();
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().onUpdateSuccess(mArticle);
                        mArticle = null;
                        getMvpView().onHideLoading();
                    }
                }));
    }
}
