package com.bodi.phearun.bodia.ui.addupdatearticle.mvp;

import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.Category;
import com.bodi.phearun.bodia.data.PostArticle;
import com.bodi.phearun.bodia.ui.base.MvpView;

import java.io.File;
import java.util.ArrayList;

public interface AddUpdateView extends MvpView {
    void onUploadArticleSuccess(Article article);
    void onUpdateSuccess(Article article);
    void onUploadImageSuccess(String Image, int type);
    void onGetAllCategorySuccess(ArrayList<Category> categories);
}
