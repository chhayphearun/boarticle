package com.bodi.phearun.bodia.ui.base;

import android.annotation.SuppressLint;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.app.MyApplication;
import com.bodi.phearun.bodia.app.di.component.ActivityComponent;

import com.bodi.phearun.bodia.app.di.component.DaggerActivityComponent;
import com.bodi.phearun.bodia.app.di.module.ActivityModule;
import com.bodi.phearun.bodia.service.ConnectivityReceiver;
import com.bodi.phearun.bodia.ui.setting.SettingActivity;
import com.bodi.phearun.bodia.util.LocalizeHelper;
import com.bodi.phearun.bodia.util.MySharedPreferences;

import javax.inject.Inject;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    private ActivityComponent mActivityComponent;
    @Inject
    SharedPreferences sharedPreferences;
    private boolean isSetTheme = true;
    private ConnectivityReceiver broadcastReceiver;
    private IntentFilter intentFilter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ((MyApplication) getApplication()).getApplicationComponent().inject(this);
        boolean theme_default = MySharedPreferences.getBoolean(sharedPreferences, SettingActivity.APP_THEME);
        if(theme_default){
            setTheme(R.style.AppTheme);
        }else{
            setTheme(R.style.DarkTheme);
        }
        super.onCreate(savedInstanceState);
        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((MyApplication) getApplication()).getApplicationComponent())
                .build();
        broadcastReceiver = new ConnectivityReceiver();
        intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/Roboto-RobotoRegular.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(broadcastReceiver!=null)
            registerReceiver(broadcastReceiver,intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(broadcastReceiver!=null)
            unregisterReceiver(broadcastReceiver);
    }

    public SharedPreferences getSharedPreferences(){
        return sharedPreferences;
    }
    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }
}
