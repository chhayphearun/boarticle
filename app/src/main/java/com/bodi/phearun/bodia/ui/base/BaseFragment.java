package com.bodi.phearun.bodia.ui.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;

import com.bodi.phearun.bodia.app.di.component.ActivityComponent;

import javax.inject.Inject;

public class BaseFragment extends Fragment {
    private BaseActivity mActivity;
    @Inject
    SharedPreferences sharedPreferences;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;

        }
    }
    public ActivityComponent getActivityComponent() {
        if (mActivity != null) {
            return mActivity.getActivityComponent();
        }
        return null;
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public SharedPreferences getSharedPreferences(){
        return  sharedPreferences;
    }
}
