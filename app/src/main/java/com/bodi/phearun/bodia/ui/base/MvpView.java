package com.bodi.phearun.bodia.ui.base;

public interface MvpView {

    void onError(String message);

    void onHideKeyboard();

    void onShowLoading();

    void onHideLoading();
}
