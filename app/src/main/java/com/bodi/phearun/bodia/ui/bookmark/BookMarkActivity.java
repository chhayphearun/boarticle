package com.bodi.phearun.bodia.ui.bookmark;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;
import android.widget.Toast;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.BookMark;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.ui.bookmark.adapter.BookMarkAdapter;
import com.bodi.phearun.bodia.ui.bookmark.mvp.BookMarkPresenter;
import com.bodi.phearun.bodia.ui.bookmark.mvp.BookMarkView;
import com.bodi.phearun.bodia.ui.main.adapter.AnimationItem;
import com.bodi.phearun.bodia.ui.main.adapter.ItemOffsetDecoration;
import com.bodi.phearun.bodia.ui.main.callback.OnClickArticle;
import com.bodi.phearun.bodia.ui.readarticle.ReadArticleActivity;
import com.bodi.phearun.bodia.util.LocaleHelper;
import com.bodi.phearun.bodia.util.MySharedPreferences;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookMarkActivity extends BaseActivity implements BookMarkView,OnClickArticleBookMark {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.noData)
    TextView noData;
    private BookMarkAdapter adapter;
    @Inject
    BookMarkPresenter<BookMarkView> presenter;

    private ArrayList<BookMark> articleBookMarks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_mark);
        ButterKnife.bind(this);
        getActivityComponent().inject(this);
        toolbar.setNavigationIcon(R.drawable.ic_arrowback_white);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.bookMarks));
        presenter.onAttach(this);
        setupRecyclerView();

        articleBookMarks = new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onGetBookMark(MySharedPreferences.getInt(getSharedPreferences(),"userID"));
    }

    private void setupRecyclerView() {
        final Context context = recyclerView.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen.default_spacing_small);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));


    }
    private void runLayoutAnimation(final RecyclerView recyclerView, final AnimationItem item) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, item.getResourceId());
        recyclerView.setLayoutAnimation(controller);
        if(recyclerView.getAdapter()!=null)
            recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));

    }
    @Override
    public void onActionSuccess(ArrayList<BookMark> articleBookMarks) {
        if (articleBookMarks.size() != 0){
            this.articleBookMarks = articleBookMarks;
            adapter = new BookMarkAdapter(recyclerView,this, this.articleBookMarks);
            AnimationItem mSelectedItem = new AnimationItem("Fall down", R.anim.layout_animation_fall_down);
            runLayoutAnimation(recyclerView,mSelectedItem);
            recyclerView.setAdapter(adapter);
            noData.setVisibility(View.GONE);
        }else{
            this.articleBookMarks.clear();
            if(adapter!=null)
                adapter.notifyDataSetChanged();
            noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void onShowLoading() {

    }

    @Override
    public void onHideLoading() {

    }


    @Override
    public void onArticleClicked(BookMark bookMark) {
        Intent intent = new Intent(this,ReadArticleActivity.class);
        intent.putExtra("bookMark",bookMark);
        intent.putExtra("isBookMark",true);
        startActivity(intent);
    }
}
