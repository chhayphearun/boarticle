package com.bodi.phearun.bodia.ui.bookmark;

import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.BookMark;

public interface OnClickArticleBookMark {
    void onArticleClicked(BookMark bookMark);
}
