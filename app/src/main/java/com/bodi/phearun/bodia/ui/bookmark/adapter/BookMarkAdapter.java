package com.bodi.phearun.bodia.ui.bookmark.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.BookMark;
import com.bodi.phearun.bodia.ui.bookmark.OnClickArticleBookMark;
import com.bodi.phearun.bodia.ui.main.callback.OnClickArticle;
import com.bodi.phearun.bodia.ui.main.callback.OnLoadMoreListener;
import com.bodi.phearun.bodia.util.animation.AnimationUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookMarkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 2;
    private static final int VIEW_TYPE_LOADING = 1;
    private List<BookMark> bookMarksList;
    private LinearLayoutManager mManager;
    public boolean loading = true;
    private boolean isLoading;
    private OnLoadMoreListener onLoadMoreListener;
    private Context context;
    private int previousPosition = 0;
    private OnClickArticleBookMark onArticleClicked;
    public BookMarkAdapter() {

    }

    public BookMarkAdapter(RecyclerView recyclerView, Context context, List<BookMark> bookMarksList) {
        this.bookMarksList = bookMarksList;
        this.context = context;
        this.mManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if(context instanceof OnClickArticleBookMark){
            onArticleClicked = (OnClickArticleBookMark)context;
        }
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mManager.getChildCount();
                int totalItemCount = mManager.getItemCount();
                int pastVisibleItems = mManager.findFirstVisibleItemPosition();
                if(totalItemCount>9){
                    if (pastVisibleItems + visibleItemCount >= totalItemCount && !isLoading) {
                        //End of list
                        if(loading){
                            if (onLoadMoreListener != null) {
                                onLoadMoreListener.onLoadMore();
                            }
                            isLoading = true;
                        }
                    }
                }
            }
        });
    }
    //disableProgressBarWhenServerReturnNoData
    public void setLoad(){
        loading = false;
    }


    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }
    //disableProgressBarAfterGetDataFromServer
    public void setLoaded() {
        isLoading = false;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new ItemViewHolder(bindViewLayout(viewGroup,R.layout.recylcer_bookmark_list));
        } else if (viewType == VIEW_TYPE_LOADING) {
            return new LoadingViewHolder(bindViewLayout(viewGroup,R.layout.recyler_footer));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
      if(viewHolder instanceof ItemViewHolder){
            final ItemViewHolder itemViewHolder = (ItemViewHolder)viewHolder;
            final Article article = bookMarksList.get(position).getARTICLE();
            itemViewHolder.title.setText(article.getTITLE());
            String cate = article.getCategory().getNAME();
            itemViewHolder.category.setText(cate);

                Glide.with(itemViewHolder.itemView.getContext())
                        .load(article.getIMAGE())
                        .apply(new RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.ic_launcher_foreground))
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                Bitmap icon = drawableToBitmap(resource);
                                article.setBitmap(icon);
                                return false;
                            }
                        })
                        .into(itemViewHolder.image);

            animate(position,itemViewHolder);
        }else if(viewHolder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = ((LoadingViewHolder) viewHolder);
            loadingViewHolder.progressBar.setIndeterminate(true);
            animate(position,loadingViewHolder);
        }
        previousPosition = viewHolder.getAdapterPosition();
    }
    private void animate(int position,RecyclerView.ViewHolder viewHolder){
        if(position > previousPosition){ // We are scrolling DOWN
            AnimationUtil.animate(viewHolder, true);
        }else{ // We are scrolling UP
            AnimationUtil.animate(viewHolder, false);
        }
    }
    private View bindViewLayout(ViewGroup viewGroup,@LayoutRes int resource){
        return LayoutInflater.from(viewGroup.getContext()).inflate(resource, viewGroup, false);
    }

    @Override
    public int getItemCount() {
        return bookMarksList == null ? 0 : bookMarksList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(bookMarksList.get(position) == null){
            return VIEW_TYPE_LOADING;
        }else{
            return VIEW_TYPE_ITEM;
        }
    }

    private Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }
        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.category)
        TextView category;
        @BindView(R.id.title)
        TextView title;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position =getAdapterPosition();
                    BookMark bookMark = bookMarksList.get(position);
                    onArticleClicked.onArticleClicked(bookMark);
                }
            });
        }
    }
    class LoadingViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.progressBar1)
        ProgressBar progressBar;
        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
