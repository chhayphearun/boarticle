package com.bodi.phearun.bodia.ui.bookmark.mvp;

import com.bodi.phearun.bodia.app.di.PerActivity;
import com.bodi.phearun.bodia.data.PostBookMark;
import com.bodi.phearun.bodia.ui.base.MvpPresenter;
import com.bodi.phearun.bodia.ui.readarticle.mvp.AddReBookMarkView;

@PerActivity
public interface BookMarkPresenter<V extends BookMarkView> extends MvpPresenter<V> {
    void onGetBookMark(long id);


}
