package com.bodi.phearun.bodia.ui.bookmark.mvp;

import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.data.BookMark;
import com.bodi.phearun.bodia.data.response.ResponseGetBookMark;
import com.bodi.phearun.bodia.ui.base.BasePresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class BookMarkPresenterImple<V extends BookMarkView> extends BasePresenter<V>
        implements BookMarkPresenter<V> {

    private ArticleService service;
    private CompositeDisposable disposable;
    @Inject
    public BookMarkPresenterImple(ArticleService service, CompositeDisposable disposable) {
        this.service = service;
        this.disposable = disposable;
    }

    private void getAllBookMark(long id){
        getMvpView().onShowLoading();
        final ArrayList<BookMark> articleBookMarks = new ArrayList<>();
        disposable.add(service.getAllBookMark(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ResponseGetBookMark>() {
                    @Override
                    public void onNext(ResponseGetBookMark response) {
                        if(response.getArticleBookMark()!=null)
                        articleBookMarks.addAll(response.getArticleBookMark()) ;
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().onActionSuccess(articleBookMarks);
                        getMvpView().onHideLoading();
                    }
                }));
    }

    @Override
    public void onGetBookMark(long id) {
        getAllBookMark(id);
    }
}
