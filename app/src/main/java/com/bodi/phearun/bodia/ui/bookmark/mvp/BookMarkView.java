package com.bodi.phearun.bodia.ui.bookmark.mvp;

import com.bodi.phearun.bodia.data.BookMark;
import com.bodi.phearun.bodia.ui.base.MvpView;

import java.util.ArrayList;

public interface BookMarkView extends MvpView {
    void onActionSuccess(ArrayList<BookMark> articleBookMarks);
//    void onLoadMoreSuccess(ArrayList<Article> articles);
}
