package com.bodi.phearun.bodia.ui.flashscreen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.data.FBUserCredential;
import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.ui.login.LoginActivity;
import com.bodi.phearun.bodia.ui.main.MainActivity;
import com.bodi.phearun.bodia.ui.setting.SettingActivity;
import com.bodi.phearun.bodia.ui.user.mvp.UserPresenter;
import com.bodi.phearun.bodia.ui.user.mvp.UserView;
import com.bodi.phearun.bodia.util.LocaleHelper;
import com.bodi.phearun.bodia.util.LocalizeHelper;
import com.bodi.phearun.bodia.util.MySharedPreferences;
import com.bodi.phearun.bodia.util.animation.TransitionHelper;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import javax.inject.Inject;

public class FlashScreenActivity extends BaseActivity implements UserView.userInfo {
    private final static int WELCOME_TIMEOUT = 500;
    ImageView imageView;
    @Inject
    UserPresenter<UserView.userInfo> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_screen);
        imageView = findViewById(R.id.image);
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        String lang;
        //defaultSize
        if(!MySharedPreferences.isShareAvailable(getSharedPreferences(),SettingActivity.FONT_SIZE)){
            float size = 14;
            MySharedPreferences.putShared(getSharedPreferences(),SettingActivity.FONT_SIZE,size);
        }

        if(!MySharedPreferences.isShareAvailable(getSharedPreferences(),SettingActivity.FONT_STYLE)){
            String khmer = "font/battambang/Battambang-Regular.ttf";
            MySharedPreferences.putShared(getSharedPreferences(),SettingActivity.FONT_STYLE, khmer);
        }
        if(!MySharedPreferences.isShareAvailable(getSharedPreferences(),SettingActivity.APP_THEME)){
            MySharedPreferences.putShared(getSharedPreferences(),SettingActivity.APP_THEME, true);
        }

        if(MySharedPreferences.isShareAvailable(getSharedPreferences(), SettingActivity.APP_LANG)){
            lang = MySharedPreferences.getString(getSharedPreferences(),SettingActivity.APP_LANG);
        }else{
            lang = "en";
            MySharedPreferences.putShared(getSharedPreferences(),SettingActivity.APP_LANG,lang);
        }
        updateViews(lang);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        if(MySharedPreferences.isShareAvailable(getSharedPreferences(),"session")){
            if(MySharedPreferences.getBoolean(getSharedPreferences(),"session")){
                presenter.onGetInfo(MySharedPreferences.getInt(getSharedPreferences(),"userID"));
            }
        } else{
            if(isLoggedIn){
                getRequest(accessToken,this,MainActivity.class);
            }else{
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        transitionToActivity(FlashScreenActivity.this,LoginActivity.class,imageView);
//                startActivity(new Intent(FlashScreenActivity.this,LoginActivity.class));
//                overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
//                finish();
                    }
                },WELCOME_TIMEOUT);
            }
        }



    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    private void updateViews(String languageCode) {
        Context context = LocaleHelper.setLocale(this, languageCode);
        context.getResources();
    }
    public void getRequest(AccessToken accessToken, final Activity activity, final Class<?> cls){
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.i("1234567890",object.toString());
                JsonParser parser = new JsonParser();
                JsonElement mJson =  parser.parse(object.toString());
                Gson gson = new Gson();
                FBUserCredential fbUser = gson.fromJson(mJson, FBUserCredential.class);
                Intent intent = new Intent(activity,cls);
                intent.putExtra("fb",true);
                intent.putExtra("user",fbUser);
                activity.startActivity(intent);
            }
        });
        Bundle bundle = new Bundle();
        bundle.putString("fields","id,first_name,last_name,picture.type(large),email");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();
    }

    private void transitionToActivity(Activity activity, Class target, ImageView viewHolder) {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(activity, false,
                new Pair<>(viewHolder, getResources().getString(R.string.app_name)));
        Intent i = new Intent(activity, target);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, pairs);
        activity.startActivity(i, transitionActivityOptions.toBundle());
    }

    @Override
    public void onGetUserInfo(User user) {
        Intent intent = new Intent(this ,MainActivity.class);
        intent.putExtra("fb",false);
        intent.putExtra("user",user);
        startActivity(intent);
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void onShowLoading() {

    }

    @Override
    public void onHideLoading() {

    }
}
