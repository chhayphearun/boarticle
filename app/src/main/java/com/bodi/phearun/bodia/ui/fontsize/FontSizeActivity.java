package com.bodi.phearun.bodia.ui.fontsize;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.ui.setting.SettingActivity;
import com.bodi.phearun.bodia.util.LocaleHelper;
import com.bodi.phearun.bodia.util.MySharedPreferences;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FontSizeActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private float defaultSize = 14;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_font_size);
        ButterKnife.bind(this);
        getActivityComponent().inject(this);
        toolbar.setNavigationIcon(R.drawable.ic_arrowback_white);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.textsize));
        final TextView textView = findViewById(R.id.text);
        if(MySharedPreferences.isShareAvailable(getSharedPreferences(), SettingActivity.FONT_SIZE)){
            defaultSize = MySharedPreferences.getFloat(getSharedPreferences(),SettingActivity.FONT_SIZE);
            textView.setTextSize(defaultSize);
        }

        final LinearLayout content = findViewById(R.id.java_build);
        String[] array = {"A", "B", "C", "D", "E", "F", "G"};
        IndicatorSeekBar discrete_ticks = IndicatorSeekBar
                .with(getApplicationContext())
                .max(30)
                .min(12)
                .progress(defaultSize)
                .tickCount(7)
                .tickMarksDrawable(getResources().getDrawable(R.drawable.indicator))
                .tickTextsArray(array)
                .showTickTexts(false)
                .tickTextsColorStateList(getResources().getColorStateList(R.color.transparent))
                .indicatorColor(getResources().getColor(R.color.transparent))
                .indicatorTextColor(getResources().getColor(R.color.transparent))
                .thumbColor(Color.parseColor("#FF878787"))
                .thumbSize(20)
                .trackProgressColor(getResources().getColor(R.color.indicator_color))
                .trackProgressSize(1)
                .trackBackgroundColor(getResources().getColor(R.color.indicator_color))
                .trackBackgroundSize(1)
                .build();
        content.addView(discrete_ticks);
        discrete_ticks.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                defaultSize = seekBar.getProgressFloat();
                textView.setTextSize(defaultSize);

            }
        });
    }
    @Override
    public void onBackPressed() {
        MySharedPreferences.putShared(getSharedPreferences(),SettingActivity.FONT_SIZE,defaultSize);
        super.onBackPressed();
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
