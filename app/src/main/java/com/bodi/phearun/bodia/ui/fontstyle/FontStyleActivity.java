package com.bodi.phearun.bodia.ui.fontstyle;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.ui.setting.SettingActivity;
import com.bodi.phearun.bodia.util.LocaleHelper;
import com.bodi.phearun.bodia.util.MySharedPreferences;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FontStyleActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.font1)
    Button font1;
    @BindView(R.id.font2)
    Button font2;
    @BindView(R.id.text)
    TextView textView;
    private String fontPath;
    private final String english = "font/open-san/OpenSans-Bold.ttf";
    private final String khmer = "font/battambang/Battambang-Regular.ttf";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_font_style);
        getActivityComponent().inject(this);
        ButterKnife.bind(this);
        toolbar.setNavigationIcon(R.drawable.ic_arrowback_white);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.font));
        if(MySharedPreferences.isShareAvailable(getSharedPreferences(), SettingActivity.FONT_STYLE)){
            if(MySharedPreferences.getString(getSharedPreferences(),SettingActivity.FONT_STYLE).equals(english)){
                setCurrentTextFont(textView,english);
                font2.setTypeface(null, Typeface.BOLD);
            }else{
                setCurrentTextFont(textView,khmer);
                font1.setTypeface(null, Typeface.BOLD);
            }
        }
    }
    @OnClick(R.id.font1)
    void setFont1(){
        fontPath = khmer;
        setCurrentTextFont(textView,fontPath);
        font1.setTypeface(null, Typeface.BOLD);
        font2.setTypeface(null, Typeface.NORMAL);
    }
    @OnClick(R.id.font2)
    void setFont2(){
        fontPath = english;
        setCurrentTextFont(textView,fontPath);
        font2.setTypeface(null, Typeface.BOLD);
        font1.setTypeface(null, Typeface.NORMAL);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));

    }
    private void setFont(String fontName){
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(fontName)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        MySharedPreferences.putShared(getSharedPreferences(),SettingActivity.FONT_STYLE,fontName);
    }
    private void setCurrentTextFont(TextView text,String name){
        Typeface font = Typeface.createFromAsset(getAssets(), name);
        text.setTypeface(font);
    }

    @Override
    public void onBackPressed() {
        setFont(fontPath);
        super.onBackPressed();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
