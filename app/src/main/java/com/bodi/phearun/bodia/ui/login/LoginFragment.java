package com.bodi.phearun.bodia.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.app.di.component.ActivityComponent;
import com.bodi.phearun.bodia.data.FBUserCredential;
import com.bodi.phearun.bodia.data.PostUser;
import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.data.response.UserResponse;
import com.bodi.phearun.bodia.ui.base.BaseFragment;
import com.bodi.phearun.bodia.ui.flashscreen.FlashScreenActivity;
import com.bodi.phearun.bodia.ui.login.mvp.LoginPresenter;
import com.bodi.phearun.bodia.ui.login.mvp.LoginView;
import com.bodi.phearun.bodia.ui.main.MainActivity;
import com.bodi.phearun.bodia.ui.signup.SignUpActivity;
import com.bodi.phearun.bodia.util.MySharedPreferences;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginFragment extends BaseFragment implements LoginView {
    private Unbinder unbinder;
    private CallbackManager callbackManager;
    private static final String EMAIL = "email";
    @BindView(R.id.login_button)
    LoginButton login_button;
    @BindView(R.id.etPassword)
    EditText password;
    @BindView(R.id.etEmail)
    EditText email;
    @BindView(R.id.text_ticker)
    TextView text_ticker;
    @Inject
    LoginPresenter<LoginView> presenter;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            presenter.onAttach(this);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        text_ticker.setSelected(true);
        login_button.setReadPermissions(Arrays.asList(EMAIL,"public_profile"));
        login_button.setFragment(this);
        // If you are using in a fragment, call loginButton.setFragment(this);
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                AccessToken accessToken = loginResult.getAccessToken();
                FlashScreenActivity flashScreenActivity = new FlashScreenActivity();
                flashScreenActivity.getRequest(accessToken,getActivity(),MainActivity.class);
            }
            @Override
            public void onCancel() {
                // App code
                Log.i("1234567890","Cancel");
            }
            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.i("1234567890",exception.toString());
            }
        });
    }

    @OnClick(R.id.btLogin)
    public void login(){
        PostUser postUser = new PostUser(email.getText().toString(),password.getText().toString());
        presenter.onLoginUser(postUser);
        
    }
    @OnClick(R.id.signUp)
    public void signUp(){
        Intent intent = new Intent(getActivity(),SignUpActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.skip)
    public void skip(){
        Intent intent = new Intent(getActivity(),MainActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.fbLogin)
    void fbLogin(){
        login_button.performClick();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onLoginSuccess(User user) {
        if(user!=null){
            Intent intent = new Intent(getActivity(),MainActivity.class);
            intent.putExtra("fb",false);
            intent.putExtra("user",user);
            if(getActivity()!=null)
            getActivity().startActivity(intent);
            MySharedPreferences.putShared(getSharedPreferences(),"session",true);
            MySharedPreferences.putShared(getSharedPreferences(),"userID",user.getID());
        }else{
            Toast.makeText(getActivity(), "Incorrect! please check your email & password", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void onShowLoading() {

    }

    @Override
    public void onHideLoading() {

    }
}
