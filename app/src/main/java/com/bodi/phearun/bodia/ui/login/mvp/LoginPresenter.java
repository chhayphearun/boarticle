package com.bodi.phearun.bodia.ui.login.mvp;

import com.bodi.phearun.bodia.app.di.PerActivity;
import com.bodi.phearun.bodia.data.PostUser;
import com.bodi.phearun.bodia.ui.base.MvpPresenter;
@PerActivity
public interface LoginPresenter <V extends LoginView> extends MvpPresenter<V> {
    void onLoginUser(PostUser user);
}
