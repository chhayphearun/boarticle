package com.bodi.phearun.bodia.ui.login.mvp;

import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.data.PostUser;
import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.data.response.UserResponse;
import com.bodi.phearun.bodia.service.ServiceGenerators;
import com.bodi.phearun.bodia.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenterImple <V extends LoginView> extends BasePresenter<V>
        implements LoginPresenter<V> {
    private User user;
    private ArticleService service;
    private CompositeDisposable disposable;
    @Inject
    public LoginPresenterImple(ArticleService service, CompositeDisposable disposable) {
        this.service = service;
        this.disposable = disposable;
    }

    @Override
    public void onLoginUser(PostUser postUser) {
        user = new User();
        getMvpView().onShowLoading();
        disposable.add(service.login(postUser)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<UserResponse>() {
                    @Override
                    public void onNext(UserResponse responseArticle) {
                        user = responseArticle.getUser();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().onLoginSuccess(user);
                        getMvpView().onHideLoading();
                    }
                }));
    }
}
