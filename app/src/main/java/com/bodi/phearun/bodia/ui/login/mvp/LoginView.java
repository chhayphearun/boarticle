package com.bodi.phearun.bodia.ui.login.mvp;

import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.data.response.UserResponse;
import com.bodi.phearun.bodia.ui.base.MvpView;

public interface LoginView extends MvpView {
    void onLoginSuccess(User user);
}
