package com.bodi.phearun.bodia.ui.main;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.data.FBUserCredential;
import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.dialog.MyDialog;
import com.bodi.phearun.bodia.ui.ViewImageActivity;
import com.bodi.phearun.bodia.ui.addupdatearticle.AddUpdateArticleActivity;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.ui.bookmark.BookMarkActivity;
import com.bodi.phearun.bodia.ui.login.LoginActivity;
import com.bodi.phearun.bodia.ui.main.fragment.ArticleFragment;
import com.bodi.phearun.bodia.ui.main.menu.DrawerAdapter;
import com.bodi.phearun.bodia.ui.main.menu.DrawerItem;
import com.bodi.phearun.bodia.ui.main.menu.SimpleItem;
import com.bodi.phearun.bodia.ui.main.menu.SpaceItem;
import com.bodi.phearun.bodia.ui.setting.SettingActivity;
import com.bodi.phearun.bodia.util.LocaleHelper;
import com.bodi.phearun.bodia.util.MySharedPreferences;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;
import com.yarolegovich.slidingrootnav.callback.DragListener;
import com.yarolegovich.slidingrootnav.callback.DragStateListener;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements DrawerAdapter.OnItemSelectedListener{
    private static final int POS_ADD_ARTICLE = 0;
    private static final int POS_BOOKMARK = 1;
    private static final int POS_SETTING = 2;
    private static final int POS_LOGOUT = 4;
    private SlidingRootNav slidingRootNav;

    private String[] screenTitles;
    private Drawable[] screenIcons;
    ValueAnimator anim;
    private ArticleFragment fragment;
    private DrawerAdapter adapter;
    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.profile_image)
    ImageView profile_image;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.layoutImage)
    LinearLayout layoutImage;
    private boolean isFBLogin = false;
    private boolean isUserLogin = false;
    private static final int REQUEST_CODE = 1;
    private int statusBarToColorWhite;
    private int statusBarToColorPrimary;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getActivityComponent().inject(this);
        Resources.Theme theme = this.getTheme();
        if(MySharedPreferences.getBoolean(getSharedPreferences(),SettingActivity.APP_THEME)){
            statusBarToColorWhite = getResources().getColor(R.color.white);
            statusBarToColorPrimary = getResources().getColor(R.color.colorPrimary);
        }else{
            statusBarToColorWhite = getResources().getColor(R.color.colorPrimaryDark_Mode);
            statusBarToColorPrimary = getResources().getColor(R.color.colorPrimaryDark_Mode);
        }
        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.layout_menu_left_drawer)
                .addDragListener(new DragListener() {
                    @Override
                    public void onDrag(float progress) {
                        anim = ValueAnimator.ofFloat(progress);
                        anim.start();
                    }
                })

                .addDragStateListener(new DragStateListener() {
                    @Override
                    public void onDragStart() {

                    }
                    @Override
                    public void onDragEnd(boolean isMenuOpened) {
                        int i = getWindow().getStatusBarColor();
                        if(isMenuOpened){
                            changeStatusBarColor(statusBarToColorPrimary,statusBarToColorWhite);
                        }else{
                            changeStatusBarColor(statusBarToColorWhite,statusBarToColorPrimary);

                        }
                        //Toast.makeText(MainActivity.this,(isMenuOpened?"yes":"no"), Toast.LENGTH_SHORT).show();
                    }
                })
                .inject();
        ButterKnife.bind(this);

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        Intent intent = getIntent();
        if(intent.getExtras()!=null){
            boolean fb = intent.getBooleanExtra("fb",false);
            if(fb){
                FBUserCredential fbUser = intent.getParcelableExtra("user");
                if(fbUser!=null){
                    isFBLogin = true;
                    Glide.with(getApplicationContext())
                            .load(fbUser.getPicture().getData().getUrl())
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.ic_launcher_foreground))
                            .into(profile_image);
                    layoutImage.setVisibility(View.VISIBLE);
                    userName.setText(fbUser.getFirst_name()+" "+fbUser.getLast_name());
                    screenTitles[4] = "Log Out";
                    screenIcons[4] = getResources().getDrawable(R.drawable.ic_logout);

                }else{
                    screenTitles[4] = "Sign In";
                    screenIcons[4] = getResources().getDrawable(R.drawable.ic_menu_login);
                }
            }else{
                User user = intent.getParcelableExtra("user");
                if(user!=null){
                    isUserLogin = true;
                    Glide.with(getApplicationContext())
                            .load(user.getImageUrl())
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.ic_launcher_foreground))
                            .into(profile_image);
                    layoutImage.setVisibility(View.VISIBLE);
                    userName.setText(user.getNAME());
                    screenTitles[4] =  getResources().getString(R.string.logOut);
                    screenIcons[4] = getResources().getDrawable(R.drawable.ic_logout);

                }else{
                    screenTitles[4] = getResources().getString(R.string.signIn);
                    screenIcons[4] = getResources().getDrawable(R.drawable.ic_menu_login);
                }
            }

            adapter = new DrawerAdapter(Arrays.asList(
                    createItemFor(POS_ADD_ARTICLE),
                    createItemFor(POS_BOOKMARK),
                    createItemFor(POS_SETTING),
                    new SpaceItem(48),
                    createItemFor(POS_LOGOUT)));
            adapter.setListener(this);

            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
            fragment =  new ArticleFragment();
            showFragment(fragment);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @OnClick(R.id.profile_image)
    void onProfileClicked(){
        Bitmap bitmap = drawableToBitmap(profile_image.getDrawable());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        Intent intent = new Intent(this,ViewImageActivity.class);
        intent.putExtra("isProfile",true);
        intent.putExtra("image",byteArray);
        startActivity(intent);
    }
    private Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }
        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(adapter!=null)
        adapter.removeSelected();
        slidingRootNav.closeMenu();
    }

    private void changeStatusBarColor(final int from, final int to){
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();
                // Apply blended color to the status bar.
                int blended = blendColors(from, to, position);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getWindow().setStatusBarColor(blended);
                }

                // Apply blended color to the ActionBar.
                blended = blendColors(from, to, position);
                ColorDrawable background = new ColorDrawable(blended);
                if(getSupportActionBar()!=null)
                getSupportActionBar().setBackgroundDrawable(background);
            }
        });
    }
    private int blendColors(int from, int to, float ratio) {
        final float inverseRatio = 1f - ratio;

        final float r = Color.red(to) * ratio + Color.red(from) * inverseRatio;
        final float g = Color.green(to) * ratio + Color.green(from) * inverseRatio;
        final float b = Color.blue(to) * ratio + Color.blue(from) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }
    @Override
    public void onItemSelected(int position) {

        if (position == POS_ADD_ARTICLE) {
            Intent intent = new Intent(this,AddUpdateArticleActivity.class);
            startActivityForResult(intent,REQUEST_CODE);
        }else if(position == POS_BOOKMARK){
            if(isFBLogin || isUserLogin){
                Intent intent = new Intent(this,BookMarkActivity.class);
                startActivity(intent);
            } else{
                FragmentManager fragmentManager = getSupportFragmentManager();
                MyDialog dialog = new MyDialog();
                dialog.show(fragmentManager,"");
            }
        }else if(position == POS_SETTING){
            Intent intent = new Intent(this,SettingActivity.class);
            startActivity(intent);
            finish();
        }else if (position == POS_LOGOUT) {
            if(isFBLogin){
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
                if(isLoggedIn){
                    LoginManager.getInstance().logOut();
                }
                OpenLogin();
            }else if(isUserLogin){
                MySharedPreferences.deleteShared(getSharedPreferences(),"session");
                MySharedPreferences.deleteShared(getSharedPreferences(),"userID");
                OpenLogin();
            } else{
                FragmentManager fragmentManager = getSupportFragmentManager();
                MyDialog dialog = new MyDialog();
                dialog.show(fragmentManager,"");
            }

        }
        if(adapter!=null)
        adapter.removeSelected();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            if(resultCode== AddUpdateArticleActivity.ADD){
                if (fragment!=null){
                    fragment.onActivityResult(requestCode,resultCode,data);
                }
            }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void OpenLogin(){
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
        finish();
    }
    private void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment,"tag")
                .commit();

    }
    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.ld_activityScreenTitles);
    }
    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIcons);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }
    @SuppressLint("ResourceType")
    private DrawerItem createItemFor(int position) {

        return new SimpleItem(screenIcons[position], screenTitles[position])
//                .withIconTint(color(R.color.textColorSecondary))

                .withSelectedIconTint(color(R.color.colorAccent))
                .withSelectedTextTint(color(R.color.colorAccent));
    }

    private int color(int res) {
        return ContextCompat.getColor(this, res);
    }


    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    protected void onDestroy() {
        getIntent().removeExtra("user");
        super.onDestroy();
    }
}
