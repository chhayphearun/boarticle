package com.bodi.phearun.bodia.ui.main.callback;

import com.bodi.phearun.bodia.data.Article;

public interface OnClickArticle {
    void onArticleClicked(Article article);
}
