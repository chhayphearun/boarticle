package com.bodi.phearun.bodia.ui.main.callback;

/**
 * Created by fiplus on 5/9/18.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
