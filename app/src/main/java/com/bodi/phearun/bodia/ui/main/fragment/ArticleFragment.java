package com.bodi.phearun.bodia.ui.main.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;
import android.widget.Toast;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.app.di.component.ActivityComponent;
import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.app.network.PushNotificationService;
import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.ArticleSort;
import com.bodi.phearun.bodia.data.onesignal.Contents;
import com.bodi.phearun.bodia.data.onesignal.Data;
import com.bodi.phearun.bodia.data.onesignal.OneSignalResponse;
import com.bodi.phearun.bodia.service.HandlerNotificationData;
import com.bodi.phearun.bodia.service.OneSignalService;
import com.bodi.phearun.bodia.ui.addupdatearticle.AddUpdateArticleActivity;
import com.bodi.phearun.bodia.ui.base.BaseFragment;
import com.bodi.phearun.bodia.ui.main.MainActivity;
import com.bodi.phearun.bodia.ui.main.adapter.AnimationItem;
import com.bodi.phearun.bodia.ui.main.adapter.ArticleAdapter;
import com.bodi.phearun.bodia.ui.main.adapter.HeaderItemDecoration;
import com.bodi.phearun.bodia.ui.main.adapter.ItemOffsetDecoration;
import com.bodi.phearun.bodia.ui.main.callback.OnClickArticle;
import com.bodi.phearun.bodia.ui.main.callback.OnClickRemoveUpdate;
import com.bodi.phearun.bodia.ui.main.callback.OnLoadMoreListener;
import com.bodi.phearun.bodia.ui.main.mvp.ArticlePresenter;
import com.bodi.phearun.bodia.ui.main.mvp.ArticleView;
import com.bodi.phearun.bodia.ui.readarticle.ReadArticleActivity;
import com.bodi.phearun.bodia.ui.setting.SettingActivity;
import com.bodi.phearun.bodia.util.MySharedPreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ArticleFragment extends BaseFragment implements ArticleView {
    @Inject
    ArticlePresenter<ArticleView> mPresenter;
    private ArrayList<ArticleSort> mArticle = new ArrayList<>();;
    private int num=1;
    private ArticleAdapter adapter;
    public static final int REQUEST_CODE = 1;
    private MainActivity activity;
    private boolean isRefresh = false;
    public static boolean isArticleFragment = false;
    @BindView(R.id.layout)
    View progressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.text_ticker)
    TextView text_ticker;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);

        }
       activity =(MainActivity) getActivity();
        int color;
        if(MySharedPreferences.getBoolean(getSharedPreferences(), SettingActivity.APP_THEME)){
            color = getResources().getColor(R.color.colorPrimary);
        }else{
            color = getResources().getColor(R.color.colorPrimaryDark_Mode);
        }
        if(getActivity()!=null)
            getActivity().getWindow().setStatusBarColor(color);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_articlelist, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        final String text = getArguments().getString(EXTRA_TEXT);
        isArticleFragment = true;
        ButterKnife.bind(this,view);
        mPresenter.onLoadArticle(num,true);
        refreshLayout.setRefreshing(true);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isRefresh = true;
                mArticle = new ArrayList<>();;
                num = 1;
                adapter = null;
                mPresenter.onLoadArticle(num,true);
                refreshLayout.setRefreshing(false);
            }
        });
        setupRecyclerView();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getActivity()!=null)
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver((onNotice),
                new IntentFilter(HandlerNotificationData.REQUEST_ACCEPT)
        );
    }
    private BroadcastReceiver onNotice= new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getExtras()!=null){
                String type =intent.getExtras().getString("type");
                String title =intent.getExtras().getString("title");
                String data = type + " : "+ title;
                text_ticker.setText(data);
                text_ticker.setVisibility(View.VISIBLE);
                text_ticker.setSelected(true);
                new CountDownTimer(10000, 1) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }
                    @Override
                    public void onFinish() {
                        text_ticker.setVisibility(View.GONE);
                    }
                }.start();
            }
        }
    };
    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(onNotice);
    }

    private void setupRecyclerView() {
        final Context context = recyclerView.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen.default_spacing_small);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
//        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));
    }
    private void runLayoutAnimation(final RecyclerView recyclerView, final AnimationItem item) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, item.getResourceId());
        recyclerView.setLayoutAnimation(controller);
        if(recyclerView.getAdapter()!=null)
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE && data != null){
            Article article = data.getParcelableExtra("article");
            if(resultCode == AddUpdateArticleActivity.UPDATE){
                int i = data.getIntExtra("position",0);
                ArrayList<Article> arrayList = mArticle.get(i).getArticlesItem();
                arrayList.set(i,article);
                mArticle.get(i).setArticlesItem(arrayList);
                adapter.notifyDataSetChanged();
            }else if (resultCode == AddUpdateArticleActivity.ADD){
                mArticle.clear();
                isRefresh = true;
                num = 1;
                adapter = null;
                mPresenter.onLoadArticle(num,true);

            }
        }
    }
    @Override
    public void onActionSuccess(final ArrayList<ArticleSort> articleSort) {

        if(adapter==null){
            mArticle = articleSort;
            adapter = new ArticleAdapter(recyclerView,getActivity(), mArticle);
//            recyclerView.addItemDecoration(new HeaderItemDecoration(getBaseActivity(),recyclerView, adapter));
//            AnimationItem mSelectedItem = new AnimationItem("Fall down", R.anim.layout_animation_fall_down);
//            runLayoutAnimation(recyclerView,mSelectedItem);
            recyclerView.setAdapter(adapter);
        }
        if(adapter!=null)
            adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    mArticle.add(null);
                    adapter.notifyItemInserted(mArticle.size() - 1);
                    num++;
                    mPresenter.onLoadArticle(num,false);
                }
            });
        if(adapter!=null)
            adapter.setOnRemoveUpdate(new OnClickRemoveUpdate() {
                @Override
                public void OnClickAction(int position, boolean isUpdate) {
                    if (isAdded() && activity != null) {
                        final MainActivity activity1 = (MainActivity)activity;
                        if(isUpdate){
                            Intent intent = new Intent(activity1,AddUpdateArticleActivity.class);
                            intent.putExtra("article", mArticle.get(position).articlesItem.get(position));
                            intent.putExtra("position",position);
                            startActivityForResult(intent,REQUEST_CODE);
                        }else{
                            mPresenter.onRemoveArticle(mArticle.get(position).articlesItem.get(position).getID(),position);
                        }
                    }
                }
            });
        if(adapter!=null)
            adapter.setOnClickArticle(new OnClickArticle() {
                @Override
                public void onArticleClicked(Article article) {
                    Intent intent = new Intent(getActivity(),ReadArticleActivity.class);
                    intent.putExtra("Article",article);
                    startActivity(intent);
                }
            });
        if(isRefresh){
            if (adapter!=null)
            adapter.setEnableLoad();
            isRefresh = false;
        }
        refreshLayout.setRefreshing(false);


    }

    @Override
    public void onPause() {
        super.onPause();
        isArticleFragment = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isArticleFragment = true;
    }

    @Override
    public void onLoadMoreSuccess(ArrayList<Article> articles) {
        if(!articles.isEmpty()){
            mArticle.remove(mArticle.size() - 1);
            adapter.notifyItemRemoved(mArticle.size());
            mArticle = ArticleSort.getHeaderListLetter(articles,false);
            adapter.notifyDataSetChanged();
        }else{
            mArticle.remove(mArticle.size() - 1);
            adapter.notifyItemRemoved(mArticle.size());
            adapter.notifyDataSetChanged();
            adapter.setLoad();
        }
        adapter.setLoaded();
    }

    @Override
    public void onRemoveSuccess(int position) {
        mArticle.remove(position);
        mArticle.get(position).articlesItem.remove(position);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void onShowLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mArticle.clear();
        if(mPresenter!=null)
        mPresenter.onDetach();
        isArticleFragment = false;
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

}
