package com.bodi.phearun.bodia.ui.main.mvp;

import com.bodi.phearun.bodia.data.ArticleSort;

public interface ArticleInteractor {
    void onLoadArticle(onLoadArticleFinish listener);
    interface onLoadArticleFinish{
        void onError(String message);
        void onActionSuccess(ArticleSort articleSort);
        void onShowLoading();
        void onHideLoading();
    }
}
