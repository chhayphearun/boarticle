package com.bodi.phearun.bodia.ui.main.mvp;

import com.bodi.phearun.bodia.app.di.PerActivity;
import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.PostArticle;
import com.bodi.phearun.bodia.ui.base.MvpPresenter;
@PerActivity
public interface ArticlePresenter <V extends ArticleView> extends MvpPresenter<V> {
    void onLoadArticle(long page,boolean isFirstLoad);
    void onRemoveArticle(long id,int position);

}
