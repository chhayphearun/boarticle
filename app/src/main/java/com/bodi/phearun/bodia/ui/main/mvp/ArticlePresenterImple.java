package com.bodi.phearun.bodia.ui.main.mvp;

import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.response.ResponseArticle;
import com.bodi.phearun.bodia.data.response.ResponseGetArticle;
import com.bodi.phearun.bodia.data.ArticleSort;
import com.bodi.phearun.bodia.ui.base.BasePresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ArticlePresenterImple<V extends ArticleView> extends BasePresenter<V>
        implements ArticlePresenter<V>{

    private ArticleService service;
    private CompositeDisposable disposable;
    @Inject
    public ArticlePresenterImple(ArticleService service, CompositeDisposable disposable) {
        this.service = service;
        this.disposable = disposable;
    }

    @Override
    public void onLoadArticle(long page,boolean isFirstLoad) {
        getAllArticle(page,isFirstLoad);
    }

    @Override
    public void onRemoveArticle(long id, final int position) {
        getMvpView().onShowLoading();
        disposable.add(service.removeArticle(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ResponseArticle>() {
                    @Override
                    public void onNext(ResponseArticle responseArticle) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().onRemoveSuccess(position);
                        getMvpView().onHideLoading();
                    }
                }));
    }

    private void getAllArticle(long page, final boolean isFirstLoad){
        final ArrayList<Article> articles = new ArrayList<>();
        disposable.add(service.getAllArticle(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ResponseGetArticle>() {
                    @Override
                    public void onNext(ResponseGetArticle responseGetArticle) {
                        if(responseGetArticle.getArticle().size() == 0){
                            articles.clear();
                        }else{
                            articles.addAll(responseGetArticle.getArticle());
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        if(isFirstLoad){
                            getMvpView().onActionSuccess(ArticleSort.getHeaderListLetter(articles,true));
                        }else{
                            getMvpView().onLoadMoreSuccess(articles);
                        }
                    }
                }));
    }

}
