package com.bodi.phearun.bodia.ui.main.mvp;

import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.ArticleSort;
import com.bodi.phearun.bodia.ui.base.MvpView;

import java.util.ArrayList;

public interface ArticleView extends MvpView {
    void onActionSuccess(ArrayList<ArticleSort> articleSort);
    void onLoadMoreSuccess(ArrayList<Article> articles);
    void onRemoveSuccess(int position);
}
