package com.bodi.phearun.bodia.ui.readarticle;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.data.Article;
import com.bodi.phearun.bodia.data.BookMark;
import com.bodi.phearun.bodia.data.PostBookMark;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.ui.readarticle.mvp.AddReBookMarkPresenter;
import com.bodi.phearun.bodia.ui.readarticle.mvp.AddReBookMarkView;
import com.bodi.phearun.bodia.ui.setting.SettingActivity;
import com.bodi.phearun.bodia.util.MySharedPreferences;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import javax.inject.Inject;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReadArticleActivity extends BaseActivity implements AddReBookMarkView {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageViewCollapsing)
    ImageView imageView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.author_name)
    TextView author_name;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.btbookmark)
    ImageButton btbookmark;
    private Article article;
    private BookMark bookMark;
    private boolean isBookMark = false;
    @Inject
    AddReBookMarkPresenter<AddReBookMarkView> presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_article);
        ButterKnife.bind(this);
        getActivityComponent().inject(this);
        toolbar.setNavigationIcon(R.drawable.ic_arrowback_white);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar_title.setText("");
        presenter.onAttach(this);
         if(MySharedPreferences.isShareAvailable(getSharedPreferences(), SettingActivity.FONT_STYLE)){
             setCurrentTextFont(description,MySharedPreferences.getString(getSharedPreferences(),SettingActivity.FONT_STYLE));
         }

        Intent intent = getIntent();
        if(intent.getExtras()!=null){
            boolean isType = intent.getBooleanExtra("tap", false);
            if(isType){
                article = intent.getParcelableExtra("readArticle");
                Glide.with(getApplicationContext())
                        .load(article.getIMAGE())
                        .apply(new RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.ic_launcher_foreground))
                        .into(imageView);
                title.setText(article.getTITLE());
                btbookmark.setImageDrawable(bookmark);
                Toast.makeText(this, article.getCREATED_DATE(), Toast.LENGTH_SHORT).show();
            }else{
                isBookMark = intent.getBooleanExtra("isBookMark",false);
                if(isBookMark){
                    bookMark = intent.getParcelableExtra("bookMark");
                    article = bookMark.getARTICLE();
                }else{
                    article = intent.getParcelableExtra("Article");
                }
                imageView.setImageBitmap(article.getBitmap());
                title.setText(article.getTITLE());
                btbookmark.setImageDrawable(bookmark);

            }
        }

        if(isBookMark){
            btbookmark.setImageDrawable(setBooked);
        }else{
            btbookmark.setImageDrawable(bookmark);
        }
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    if(article!=null)
                    toolbar_title.setText(article.getTITLE());
                    isShow = true;
                } else if(isShow) {
                    toolbar_title.setText("");//careful there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }
    private void setCurrentTextFont(TextView text,String name){
        Typeface font = Typeface.createFromAsset(getAssets(), name);
        text.setTypeface(font);
        float size = MySharedPreferences.getFloat(getSharedPreferences(),SettingActivity.FONT_SIZE);
        description.setTextSize(size);
        description.setText(getResources().getString(R.string.text));
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @OnClick(R.id.btShare)
    void shareContent(){
        //implicit intent send text
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://developers.facebook.com"))
                .build();
        ShareDialog.show(this,content);
    }

    @BindDrawable(R.drawable.ic_bookmark_blue)
    Drawable setBooked;
    @BindDrawable(R.drawable.ic_bookmark_white)
    Drawable bookmark;
    @OnClick(R.id.btbookmark)
    void addBookMark(){
        if( btbookmark.getDrawable() == bookmark){
            presenter.onAddBookMark(new PostBookMark(article.getID(),MySharedPreferences.getInt(getSharedPreferences(),"userID")));
        }else{
            if(bookmark!=null)
            presenter.onRemoveBookmark(bookMark.getID(),1);
        }
    }
    
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(getIntent().getExtras()!=null){
            getIntent().getExtras().clear();
        }

    }

    @Override
    public void onAddBookMarkSuccess() {
        btbookmark.setImageDrawable(setBooked);
        Toast.makeText(this, "add bookMark", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRemoveBookMarkSuccess() {
        btbookmark.setImageDrawable(bookmark);
        Toast.makeText(this, "not bookMark", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void onShowLoading() {

    }

    @Override
    public void onHideLoading() {

    }
}
