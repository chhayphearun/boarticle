package com.bodi.phearun.bodia.ui.readarticle.mvp;

import com.bodi.phearun.bodia.app.di.PerActivity;
import com.bodi.phearun.bodia.data.PostBookMark;
import com.bodi.phearun.bodia.ui.base.MvpPresenter;
import com.bodi.phearun.bodia.ui.main.mvp.ArticleView;

@PerActivity
public interface AddReBookMarkPresenter<V extends AddReBookMarkView> extends MvpPresenter<V> {
    void onAddBookMark(PostBookMark bookMark);
    void onRemoveBookmark(long id, int position);

}
