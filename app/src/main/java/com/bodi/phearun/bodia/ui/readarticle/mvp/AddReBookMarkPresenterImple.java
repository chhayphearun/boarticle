package com.bodi.phearun.bodia.ui.readarticle.mvp;

import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.data.PostBookMark;
import com.bodi.phearun.bodia.data.response.ResponseArticle;
import com.bodi.phearun.bodia.data.response.ReponsePostBookMark;
import com.bodi.phearun.bodia.data.response.ResponseBookMark;
import com.bodi.phearun.bodia.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class AddReBookMarkPresenterImple<V extends AddReBookMarkView> extends BasePresenter<V>
        implements AddReBookMarkPresenter<V> {

    private ArticleService service;
    private CompositeDisposable disposable;
    @Inject
    public AddReBookMarkPresenterImple(ArticleService service, CompositeDisposable disposable) {
        this.service = service;
        this.disposable = disposable;
    }

    private void removeBookMark(long id,int position){
        getMvpView().onShowLoading();
        disposable.add(service.removeBookMark(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ResponseBookMark>() {
                    @Override
                    public void onNext(ResponseBookMark responseArticle) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().onRemoveBookMarkSuccess();
                        getMvpView().onHideLoading();
                    }
                }));
    }

    private void addBookMark(PostBookMark bookMark){
        getMvpView().onShowLoading();
        disposable.add(service.addBookMark(bookMark)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ReponsePostBookMark>() {
                    @Override
                    public void onNext(ReponsePostBookMark responseArticle) {

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                            getMvpView().onAddBookMarkSuccess();
                    }
                }));
    }

    @Override
    public void onAddBookMark(PostBookMark bookMark) {
        addBookMark(bookMark);
    }

    @Override
    public void onRemoveBookmark(long id, int position) {
        removeBookMark(id,position);
    }
}
