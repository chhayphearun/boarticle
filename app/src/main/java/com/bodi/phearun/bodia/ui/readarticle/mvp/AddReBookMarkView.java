package com.bodi.phearun.bodia.ui.readarticle.mvp;

import com.bodi.phearun.bodia.ui.base.MvpView;

public interface AddReBookMarkView extends MvpView {
        void onAddBookMarkSuccess();
        void onRemoveBookMarkSuccess();
}
