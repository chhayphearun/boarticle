package com.bodi.phearun.bodia.ui.setting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bodi.phearun.bodia.R;

import java.util.ArrayList;

public class CustomAdatper extends BaseAdapter {
    private ArrayList<String> arrayList;
    private LayoutInflater inflater;
    private Context context;
    private int pos[];
    //Create constructor
    public CustomAdatper(Context context, ArrayList<String> arrayList,int pos[]) {
        this.arrayList = arrayList;
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.pos = pos;
    }
    //return number of item
    @Override
    public int getCount() {
        return arrayList.size();
    }
    //bind custom view to list
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View v = inflater.inflate(R.layout.item_custom_list,null);
        TextView text = v.findViewById(R.id.text);
        View viewLine = v.findViewById(R.id.viewLine);
        text.setText(arrayList.get(i));
        if(i==0){
            viewLine.setVisibility(View.VISIBLE);
        }else if(pos[0]==i){
            viewLine.setVisibility(View.VISIBLE);
        }else if(pos[1]==i){
            viewLine.setVisibility(View.VISIBLE);
        }else{
            viewLine.setVisibility(View.GONE);
        }
        return v;
    }
    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


}
