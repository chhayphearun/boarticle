package com.bodi.phearun.bodia.ui.setting;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.dialog.DialogItem;
import com.bodi.phearun.bodia.dialog.onDialogItemClick;
import com.bodi.phearun.bodia.ui.about.AboutActivity;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.ui.fontsize.FontSizeActivity;
import com.bodi.phearun.bodia.ui.fontstyle.FontStyleActivity;
import com.bodi.phearun.bodia.ui.main.MainActivity;
import com.bodi.phearun.bodia.util.LocaleHelper;
import com.bodi.phearun.bodia.util.MySharedPreferences;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingActivity extends BaseActivity implements onDialogItemClick {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.listview)
    ListView list;
    public static final int LANGUAGES = 2;
    public static final int THEME_APP = 1;
    private int k;
    private CustomAdatper customAdatper;
    private ArrayList<String> arrayList;
    private ArrayList<String> itemList;
    public static final String FONT_SIZE = "font@size";
    public static final String FONT_STYLE = "font@Style";
    public static final String APP_LANG = "appLang";
    public static final String APP_THEME = "appTheme";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView( R.layout.activity_setting);
        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.ic_arrowback_white);
        setSupportActionBar(toolbar);
        setTitle(getResources().getString(R.string.setting));
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        arrayList = new ArrayList<>();
        arrayList.add(getResources().getString(R.string.themes));
        arrayList.add(getResources().getString(R.string.notification));
        arrayList.add(getResources().getString(R.string.editinfo));
        arrayList.add(getResources().getString(R.string.textsize));
        arrayList.add(getResources().getString(R.string.font));
        arrayList.add(getResources().getString(R.string.language));
        arrayList.add(getResources().getString(R.string.aboutUs));
        final int pos[] = {1,5};
        customAdatper = new CustomAdatper(this,arrayList,pos);
        list.setAdapter(customAdatper);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==6){
                    openActivity(AboutActivity.class);
                }else if(position==3){
                    openActivity(FontSizeActivity.class);
                }else if(position==4){
                    openActivity(FontStyleActivity.class);
                }else if(position == 5){
                    String lang = MySharedPreferences.getString(getSharedPreferences(),APP_LANG);
                    if(lang.equals("km")){
                        k = 0;
                    }else if (lang.equals("en")){
                        k = 1;
                    }
                    itemList = new ArrayList<>();
                    itemList.add(khmer);
                    itemList.add(english);
                    openBottomDialog(itemList,k,LANGUAGES);
                }else if(position==0){
                    boolean theme_default = MySharedPreferences.getBoolean(getSharedPreferences(),APP_THEME);
                    if(theme_default){
                        k = 0;
                    }else{
                        k = 1;
                    }
                    itemList = new ArrayList<>();
                    itemList.add(getResources().getString(R.string.defaul));
                    itemList.add(getResources().getString(R.string.darkMode));
                    openBottomDialog(itemList,k,THEME_APP);
                }else if(position == 1){
                    Toast.makeText(SettingActivity.this, "developing", Toast.LENGTH_SHORT).show();
                }else if(position == 2){
                    Toast.makeText(SettingActivity.this, "developing", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    @BindString(R.string.khmer)
    String khmer;
    @BindString(R.string.english)
    String english;
    private void openBottomDialog(ArrayList<String> list,int position,int type){
        DialogItem bottomDialog = DialogItem.openDialog(list,position,type);
        FragmentManager fragmentManager = getSupportFragmentManager();
        bottomDialog.show(fragmentManager,"");
    }
    private void openActivity(Class<?> cls){
        Intent intent = new Intent(this,cls);
        startActivity(intent);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onItemClicked(int position,int type) {
        if(type == LANGUAGES){
            if(position==0){
                MySharedPreferences.putShared(getSharedPreferences(),APP_LANG,"km");
                updateViews("km");
            }else if(position==1){
                MySharedPreferences.putShared(getSharedPreferences(),APP_LANG,"en");
                updateViews("en");
            }
        }else if(type == THEME_APP){
            if(position==0){
                MySharedPreferences.putShared(getSharedPreferences(),APP_THEME,true);
                this.recreate();
            }else if(position==1){
                MySharedPreferences.putShared(getSharedPreferences(),APP_THEME,false);
                this.recreate();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }
    private void updateViews(String languageCode) {
        Context context = LocaleHelper.setLocale(this, languageCode);
        arrayList.clear();
        arrayList.add(context.getResources().getString(R.string.themes));
        arrayList.add(context.getResources().getString(R.string.notification));
        arrayList.add(context.getResources().getString(R.string.editinfo));
        arrayList.add(context.getResources().getString(R.string.textsize));
        arrayList.add(context.getResources().getString(R.string.font));
        arrayList.add(context.getResources().getString(R.string.language));
        arrayList.add(context.getResources().getString(R.string.aboutUs));
        customAdatper.notifyDataSetChanged();
    }

}
