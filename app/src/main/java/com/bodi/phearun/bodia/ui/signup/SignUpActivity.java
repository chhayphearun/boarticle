package com.bodi.phearun.bodia.ui.signup;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;


import com.bodi.phearun.bodia.R;
import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.dialog.OnClickOkButton;
import com.bodi.phearun.bodia.dialog.SimpleDialogMessage;
import com.bodi.phearun.bodia.ui.base.BaseActivity;
import com.bodi.phearun.bodia.ui.signup.mvp.SignUpPresenter;
import com.bodi.phearun.bodia.ui.signup.mvp.SignUpView;
import com.bodi.phearun.bodia.util.LocaleHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseActivity implements SignUpView,OnClickOkButton {
    @BindView(R.id.etUsername)
    EditText userName;
    @BindView(R.id.etEmail)
    EditText email;
    @BindView(R.id.etPassword)
    EditText password;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Inject
    SignUpPresenter<SignUpView> presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        toolbar.setNavigationIcon(R.drawable.ic_arrowback_white);
        setSupportActionBar(toolbar);
        setTitle("");
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivityComponent().inject(this);
        presenter.onAttach(this);

    }
    @OnClick(R.id.btSignUp)
    void onSignUp(){
        String name = userName.getText().toString();
        String mEmail = email.getText().toString();
        String pwd = password.getText().toString();

        presenter.onSignUpUser(name,mEmail,pwd,createFileFromResource(R.drawable.ic_face,"null"));
    }
    public File createFileFromResource(int resId, String fileName)
    {
        File f;
        try
        {
            f = new File(getCacheDir() + File.separator + fileName);
            InputStream is = getResources().openRawResource(resId);
            OutputStream out = new FileOutputStream(f);

            int bytesRead;
            byte[] buffer = new byte[1024];
            while((bytesRead = is.read(buffer)) > 0)
            {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            is.close();
        }
        catch (IOException ex)
        {
            f = null;
        }
        return f;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
    @Override
    public void onSignUpSuccess(User user) {
        if(user!=null){
            SimpleDialogMessage fragment =  SimpleDialogMessage.openDialog("Congratulations");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragment.show(fragmentManager,"");
            userName.setText("");
            email.setText("");
            password.setText("");
            password.clearFocus();
        } else {
            Toast.makeText(this, "signUp error!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void onShowLoading() {

    }

    @Override
    public void onHideLoading() {

    }

    @Override
    public void onClickOK() {
        finish();
    }
}
