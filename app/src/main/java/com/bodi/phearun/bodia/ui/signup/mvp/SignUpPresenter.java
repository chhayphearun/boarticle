package com.bodi.phearun.bodia.ui.signup.mvp;

import com.bodi.phearun.bodia.app.di.PerActivity;
import com.bodi.phearun.bodia.ui.base.MvpPresenter;

import java.io.File;

@PerActivity
public interface SignUpPresenter <V extends SignUpView> extends MvpPresenter<V>{
    void onSignUpUser(String userName,String email,String password,File file);
}
