package com.bodi.phearun.bodia.ui.signup.mvp;

import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.data.response.Responsees;
import com.bodi.phearun.bodia.data.response.UserResponse;
import com.bodi.phearun.bodia.ui.base.BasePresenter;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpPresenterImple <V extends SignUpView> extends BasePresenter<V>
        implements SignUpPresenter<V>{
    private ArticleService service;
    private CompositeDisposable disposable;
    private User user;
    @Inject
    public SignUpPresenterImple(ArticleService service,CompositeDisposable disposable) {
        this.service = service;
        this.disposable = disposable;
    }

    @Override
    public void onSignUpUser(String userName, String email, String password,File file) {
        onSignUp(userName,email,password,file);
    }
    private void onSignUp(String userName, String email, String password,File file){
        RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file /* file name*/);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("PHOTO", file.getName(), fileBody);
        disposable.add(service.signUp(email,userName,password,"null","null","null",filePart,fileBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<UserResponse>() {
                    @Override
                    public void onNext(UserResponse response) {
                        user = response.getUser();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().onSignUpSuccess(user);
                    }
                }));

    }

}
