package com.bodi.phearun.bodia.ui.signup.mvp;

import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.ui.base.MvpView;

public interface SignUpView extends MvpView {
    void onSignUpSuccess(User user);
}
