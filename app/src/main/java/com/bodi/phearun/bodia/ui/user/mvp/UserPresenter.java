package com.bodi.phearun.bodia.ui.user.mvp;

import com.bodi.phearun.bodia.app.di.PerActivity;
import com.bodi.phearun.bodia.ui.base.BasePresenter;
import com.bodi.phearun.bodia.ui.base.MvpPresenter;

@PerActivity
public interface UserPresenter <V extends UserView.userInfo> extends MvpPresenter<V> {
    void onGetInfo(long id);
}
