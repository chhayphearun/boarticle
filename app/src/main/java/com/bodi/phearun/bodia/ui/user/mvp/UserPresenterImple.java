package com.bodi.phearun.bodia.ui.user.mvp;

import com.bodi.phearun.bodia.app.network.ArticleService;
import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.data.response.UserResponse;
import com.bodi.phearun.bodia.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class UserPresenterImple <V extends UserView.userInfo> extends BasePresenter<V> implements UserPresenter<V> {
    private ArticleService service;
    private User user;
    private CompositeDisposable disposable;
    @Inject
    public UserPresenterImple(ArticleService service,CompositeDisposable disposable) {
        this.service = service;
        this.disposable = disposable;
    }

    @Override
    public void onGetInfo(long id) {
        getMvpView().onShowLoading();
        disposable.add(service.getUserInfo(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<UserResponse>() {
                    @Override
                    public void onNext(UserResponse responseArticle) {
                        if(responseArticle.getUser()!=null){
                            user = responseArticle.getUser();
                        }else{
                            getMvpView().onGetUserInfo(null);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().onError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().onGetUserInfo(user);
                        getMvpView().onHideLoading();
                    }
                }));
    }
}
