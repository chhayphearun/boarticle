package com.bodi.phearun.bodia.ui.user.mvp;

import com.bodi.phearun.bodia.data.User;
import com.bodi.phearun.bodia.ui.base.MvpView;

public interface UserView extends MvpView {
    interface userInfo extends MvpView{
        void onGetUserInfo(User user);
    }
}
