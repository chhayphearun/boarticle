package com.bodi.phearun.bodia.util;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;

import java.util.Locale;

public class LocalizeHelper {
    private AppCompatActivity appCompatActivity;

    public LocalizeHelper(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    private void restLanguage(String language){
        Locale locale = new Locale(language);
        Configuration configuration = appCompatActivity.getResources().getConfiguration();
        configuration.setLocale(locale);
        appCompatActivity.getResources().updateConfiguration(configuration,appCompatActivity.getResources().getDisplayMetrics());
        appCompatActivity.recreate();
    }
    public void changLanguage(String language){
        restLanguage(language);
    }
}
