package com.bodi.phearun.bodia.util;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreferences {
    public static void putShared(SharedPreferences sharedPreferences, String key, String data){
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString(key,data);
        editor.apply();
    }
    public static void putShared(SharedPreferences sharedPreferences, String key, int data){
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putInt(key,data);
        editor.apply();
    }
    public static void putShared(SharedPreferences sharedPreferences, String key, boolean data){
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putBoolean(key,data);
        editor.apply();
    }
    public static void putShared(SharedPreferences sharedPreferences, String key, float data){
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putFloat(key,data);

        editor.apply();
    }
    public static String getString(SharedPreferences sharedPreferences, String key){
        return sharedPreferences.getString(key,"");
    }
    public static int getInt(SharedPreferences sharedPreferences, String key){
        return sharedPreferences.getInt(key,0);
    }
    public static float getFloat(SharedPreferences sharedPreferences, String key){
        return sharedPreferences.getFloat(key,0);
    }
    public static boolean getBoolean(SharedPreferences sharedPreferences, String key){
        return sharedPreferences.getBoolean(key,false);
    }
    public static boolean isShareAvailable(SharedPreferences sharedPreferences,String key){
        return sharedPreferences.contains(key);
    }
    public static void deleteShared(SharedPreferences sharedPreferences,String key){
        sharedPreferences.edit().remove(key).apply();
    }
}
